#!/usr/bin/env bash

ZEUS_DIR="tools/zeus"
BUILD_DIR="bin/build"
INTERMEDIATE_DIR="${BUILD_DIR}/ir"

git_version=$(git describe --long --tags --dirty --always)
build_name="snake-${git_version}"

if ! [ -d "$BUILD_DIR" ]; then
    mkdir -p "$BUILD_DIR"
fi

if [ -d "$INTERMEDIATE_DIR" ]; then
    rm -r -- "$INTERMEDIATE_DIR"
fi
mkdir "$INTERMEDIATE_DIR"

echo Building $build_name...

if [ "$1" != "--no-wine" ]; then
    USE_WINE='wine'
fi

(cd src && exec ${USE_WINE} ../${ZEUS_DIR}/zcl.exe ld_snake.asm)

${USE_WINE} ./tools/bas2tap/bas2tap.exe -a10 "-sLD-Snake" src/loader.bas ${INTERMEDIATE_DIR}/loader.tap

if ! [ -f "${INTERMEDIATE_DIR}/loader.tap" ]; then
    echo -e '\nERROR building Snake!'
    exit 1
fi

${USE_WINE} ./tools/GenTape/GenTape.exe ${INTERMEDIATE_DIR}/game.tap \
    hdata "'LD-Snake `echo AA | xxd -r -p`'" 4000 src/snake.scr \
    hdata "'`echo 16030108 | xxd -r -p`Bytes:'" 8000 ${INTERMEDIATE_DIR}/game.bin \

if ! [ -f "${INTERMEDIATE_DIR}/game.tap" ]; then
    echo -e '\nERROR building Snake!'
    exit 1
fi

cat ${INTERMEDIATE_DIR}/loader.tap ${INTERMEDIATE_DIR}/game.tap > ${BUILD_DIR}/${build_name}.tap

echo -e '\nSnake build successful.'
