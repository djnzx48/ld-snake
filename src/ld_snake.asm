zeusemulate "48K"
zoLogicOperatorsHighPri = false

head_colour     equ 4*9 + 64 ; bright green
snake_colour    equ 4*9      ; green
food_colour     equ 6*9 + 64 ; bright yellow
next_lev_colour equ 3*9      ; magenta
obstacle_colour equ 5*9 + 64 ; bright cyan
empty_colour    equ 0        ; black
border_colour   equ 5*9      ; cyan

starting_level  equ HIGH level0
last_level      equ (HIGH level2) + 1

no_key          equ (HIGH up_tab) + 1 ; ensure it does not match any key table values

start           equ $8000 ; arbitrary
main            equ $AFF3 ; DO NOT CHANGE THESE!!!
gameloop        equ $C3FF ;
delayloop       equ $CBC3 ;

; stack LSB values
goto_stack      equ 0
goto_main       equ 2
goto_loop       equ 6
goto_delayloop  equ 7

stack_size      equ 50 ; arbitrary number of bytes
stack           equ $0000
stack_start     equ stack - stack_size

score_screen    equ $50AC
hiscore_screen  equ $50B0

screen          equ $5900

; addresses of chars in ROM font
; high byte:
font            equ $3D
; low bytes:
font_sp         equ $00
font_0          equ $80
font_1          equ $88
font_2          equ $90
font_3          equ $98
font_4          equ $A0
font_5          equ $A8
font_6          equ $B0
font_7          equ $B8
font_8          equ $C0
font_9          equ $C8

LAST_K          equ $5C08
FRAMES          equ $5C78

; lookup tables
level0          equ $6000
level1          equ $6100
level2          equ $6200

screen_tab      equ $6F00

rand_tab        equ $7100

opposite_dir_tab equ $7200

snake_array     equ $7300

right_tab       equ $7400
down_tab        equ $7500
left_tab        equ $7600
up_tab          equ $7700

key_tab         equ $7800

inc_tab         equ $7900
dec_tab         equ $7A00

scratch         equ $7B00

digit_tab       equ $7C00

org $7F00

new_hi:
    ds 1

old_hi:
    ds 1

score_and_food_flag:
score:
    ds 1
food_flag:
    ds 1

head_and_tail_ptr:
head:
    ds 1
tail:
    ds 1

direction:
    ds 1

grow_flag:
    ds 1

temp_pos:
    ds 1

rand_ptr:
    ds 1

delay_counter:
    ds 1

delay_length:
    ds 1

die_sp:
    ds 1

next_lev_counter:
    ds 1

next_lev_flag:
    ds 1

draw_food_colour:
    ds 1

current_level:
    ds 1

org start
zoCheckORG = true
    ; generate LUTs

    ; right_tab
    ld hl, $0201: ld (right_tab + $00), hl
    ld hl, $0403: ld (right_tab + $02), hl
    ld hl, $0605: ld (right_tab + $04), hl
    ld hl, $0807: ld (right_tab + $06), hl
    ld hl, $0A09: ld (right_tab + $08), hl
    ld hl, $0C0B: ld (right_tab + $0A), hl
    ld hl, $0E0D: ld (right_tab + $0C), hl
    ld hl, $000F: ld (right_tab + $0E), hl
    ld hl, $1211: ld (right_tab + $10), hl
    ld hl, $1413: ld (right_tab + $12), hl
    ld hl, $1615: ld (right_tab + $14), hl
    ld hl, $1817: ld (right_tab + $16), hl
    ld hl, $1A19: ld (right_tab + $18), hl
    ld hl, $1C1B: ld (right_tab + $1A), hl
    ld hl, $1E1D: ld (right_tab + $1C), hl
    ld hl, $101F: ld (right_tab + $1E), hl

    ld hl, $2221: ld (right_tab + $20), hl
    ld hl, $2423: ld (right_tab + $22), hl
    ld hl, $2625: ld (right_tab + $24), hl
    ld hl, $2827: ld (right_tab + $26), hl
    ld hl, $2A29: ld (right_tab + $28), hl
    ld hl, $2C2B: ld (right_tab + $2A), hl
    ld hl, $2E2D: ld (right_tab + $2C), hl
    ld hl, $202F: ld (right_tab + $2E), hl
    ld hl, $3231: ld (right_tab + $30), hl
    ld hl, $3433: ld (right_tab + $32), hl
    ld hl, $3635: ld (right_tab + $34), hl
    ld hl, $3837: ld (right_tab + $36), hl
    ld hl, $3A39: ld (right_tab + $38), hl
    ld hl, $3C3B: ld (right_tab + $3A), hl
    ld hl, $3E3D: ld (right_tab + $3C), hl
    ld hl, $303F: ld (right_tab + $3E), hl

    ld hl, $4241: ld (right_tab + $40), hl
    ld hl, $4443: ld (right_tab + $42), hl
    ld hl, $4645: ld (right_tab + $44), hl
    ld hl, $4847: ld (right_tab + $46), hl
    ld hl, $4A49: ld (right_tab + $48), hl
    ld hl, $4C4B: ld (right_tab + $4A), hl
    ld hl, $4E4D: ld (right_tab + $4C), hl
    ld hl, $404F: ld (right_tab + $4E), hl
    ld hl, $5251: ld (right_tab + $50), hl
    ld hl, $5453: ld (right_tab + $52), hl
    ld hl, $5655: ld (right_tab + $54), hl
    ld hl, $5857: ld (right_tab + $56), hl
    ld hl, $5A59: ld (right_tab + $58), hl
    ld hl, $5C5B: ld (right_tab + $5A), hl
    ld hl, $5E5D: ld (right_tab + $5C), hl
    ld hl, $505F: ld (right_tab + $5E), hl

    ld hl, $6261: ld (right_tab + $60), hl
    ld hl, $6463: ld (right_tab + $62), hl
    ld hl, $6665: ld (right_tab + $64), hl
    ld hl, $6867: ld (right_tab + $66), hl
    ld hl, $6A69: ld (right_tab + $68), hl
    ld hl, $6C6B: ld (right_tab + $6A), hl
    ld hl, $6E6D: ld (right_tab + $6C), hl
    ld hl, $606F: ld (right_tab + $6E), hl
    ld hl, $7271: ld (right_tab + $70), hl
    ld hl, $7473: ld (right_tab + $72), hl
    ld hl, $7675: ld (right_tab + $74), hl
    ld hl, $7877: ld (right_tab + $76), hl
    ld hl, $7A79: ld (right_tab + $78), hl
    ld hl, $7C7B: ld (right_tab + $7A), hl
    ld hl, $7E7D: ld (right_tab + $7C), hl
    ld hl, $707F: ld (right_tab + $7E), hl

    ld hl, $8281: ld (right_tab + $80), hl
    ld hl, $8483: ld (right_tab + $82), hl
    ld hl, $8685: ld (right_tab + $84), hl
    ld hl, $8887: ld (right_tab + $86), hl
    ld hl, $8A89: ld (right_tab + $88), hl
    ld hl, $8C8B: ld (right_tab + $8A), hl
    ld hl, $8E8D: ld (right_tab + $8C), hl
    ld hl, $808F: ld (right_tab + $8E), hl
    ld hl, $9291: ld (right_tab + $90), hl
    ld hl, $9493: ld (right_tab + $92), hl
    ld hl, $9695: ld (right_tab + $94), hl
    ld hl, $9897: ld (right_tab + $96), hl
    ld hl, $9A99: ld (right_tab + $98), hl
    ld hl, $9C9B: ld (right_tab + $9A), hl
    ld hl, $9E9D: ld (right_tab + $9C), hl
    ld hl, $909F: ld (right_tab + $9E), hl

    ld hl, $A2A1: ld (right_tab + $A0), hl
    ld hl, $A4A3: ld (right_tab + $A2), hl
    ld hl, $A6A5: ld (right_tab + $A4), hl
    ld hl, $A8A7: ld (right_tab + $A6), hl
    ld hl, $AAA9: ld (right_tab + $A8), hl
    ld hl, $ACAB: ld (right_tab + $AA), hl
    ld hl, $AEAD: ld (right_tab + $AC), hl
    ld hl, $A0AF: ld (right_tab + $AE), hl
    ld hl, $B2B1: ld (right_tab + $B0), hl
    ld hl, $B4B3: ld (right_tab + $B2), hl
    ld hl, $B6B5: ld (right_tab + $B4), hl
    ld hl, $B8B7: ld (right_tab + $B6), hl
    ld hl, $BAB9: ld (right_tab + $B8), hl
    ld hl, $BCBB: ld (right_tab + $BA), hl
    ld hl, $BEBD: ld (right_tab + $BC), hl
    ld hl, $B0BF: ld (right_tab + $BE), hl

    ld hl, $C2C1: ld (right_tab + $C0), hl
    ld hl, $C4C3: ld (right_tab + $C2), hl
    ld hl, $C6C5: ld (right_tab + $C4), hl
    ld hl, $C8C7: ld (right_tab + $C6), hl
    ld hl, $CAC9: ld (right_tab + $C8), hl
    ld hl, $CCCB: ld (right_tab + $CA), hl
    ld hl, $CECD: ld (right_tab + $CC), hl
    ld hl, $C0CF: ld (right_tab + $CE), hl
    ld hl, $D2D1: ld (right_tab + $D0), hl
    ld hl, $D4D3: ld (right_tab + $D2), hl
    ld hl, $D6D5: ld (right_tab + $D4), hl
    ld hl, $D8D7: ld (right_tab + $D6), hl
    ld hl, $DAD9: ld (right_tab + $D8), hl
    ld hl, $DCDB: ld (right_tab + $DA), hl
    ld hl, $DEDD: ld (right_tab + $DC), hl
    ld hl, $D0DF: ld (right_tab + $DE), hl

    ld hl, $E2E1: ld (right_tab + $E0), hl
    ld hl, $E4E3: ld (right_tab + $E2), hl
    ld hl, $E6E5: ld (right_tab + $E4), hl
    ld hl, $E8E7: ld (right_tab + $E6), hl
    ld hl, $EAE9: ld (right_tab + $E8), hl
    ld hl, $ECEB: ld (right_tab + $EA), hl
    ld hl, $EEED: ld (right_tab + $EC), hl
    ld hl, $E0EF: ld (right_tab + $EE), hl
    ld hl, $F2F1: ld (right_tab + $F0), hl
    ld hl, $F4F3: ld (right_tab + $F2), hl
    ld hl, $F6F5: ld (right_tab + $F4), hl
    ld hl, $F8F7: ld (right_tab + $F6), hl
    ld hl, $FAF9: ld (right_tab + $F8), hl
    ld hl, $FCFB: ld (right_tab + $FA), hl
    ld hl, $FEFD: ld (right_tab + $FC), hl
    ld hl, $F0FF: ld (right_tab + $FE), hl

    ; down_tab
    ld hl, $2120: ld (down_tab + $00), hl
    ld hl, $2322: ld (down_tab + $02), hl
    ld hl, $2524: ld (down_tab + $04), hl
    ld hl, $2726: ld (down_tab + $06), hl
    ld hl, $2928: ld (down_tab + $08), hl
    ld hl, $2B2A: ld (down_tab + $0A), hl
    ld hl, $2D2C: ld (down_tab + $0C), hl
    ld hl, $2F2E: ld (down_tab + $0E), hl
    ld hl, $3130: ld (down_tab + $10), hl
    ld hl, $3332: ld (down_tab + $12), hl
    ld hl, $3534: ld (down_tab + $14), hl
    ld hl, $3736: ld (down_tab + $16), hl
    ld hl, $3938: ld (down_tab + $18), hl
    ld hl, $3B3A: ld (down_tab + $1A), hl
    ld hl, $3D3C: ld (down_tab + $1C), hl
    ld hl, $3F3E: ld (down_tab + $1E), hl

    ld hl, $4140: ld (down_tab + $20), hl
    ld hl, $4342: ld (down_tab + $22), hl
    ld hl, $4544: ld (down_tab + $24), hl
    ld hl, $4746: ld (down_tab + $26), hl
    ld hl, $4948: ld (down_tab + $28), hl
    ld hl, $4B4A: ld (down_tab + $2A), hl
    ld hl, $4D4C: ld (down_tab + $2C), hl
    ld hl, $4F4E: ld (down_tab + $2E), hl
    ld hl, $5150: ld (down_tab + $30), hl
    ld hl, $5352: ld (down_tab + $32), hl
    ld hl, $5554: ld (down_tab + $34), hl
    ld hl, $5756: ld (down_tab + $36), hl
    ld hl, $5958: ld (down_tab + $38), hl
    ld hl, $5B5A: ld (down_tab + $3A), hl
    ld hl, $5D5C: ld (down_tab + $3C), hl
    ld hl, $5F5E: ld (down_tab + $3E), hl

    ld hl, $6160: ld (down_tab + $40), hl
    ld hl, $6362: ld (down_tab + $42), hl
    ld hl, $6564: ld (down_tab + $44), hl
    ld hl, $6766: ld (down_tab + $46), hl
    ld hl, $6968: ld (down_tab + $48), hl
    ld hl, $6B6A: ld (down_tab + $4A), hl
    ld hl, $6D6C: ld (down_tab + $4C), hl
    ld hl, $6F6E: ld (down_tab + $4E), hl
    ld hl, $7170: ld (down_tab + $50), hl
    ld hl, $7372: ld (down_tab + $52), hl
    ld hl, $7574: ld (down_tab + $54), hl
    ld hl, $7776: ld (down_tab + $56), hl
    ld hl, $7978: ld (down_tab + $58), hl
    ld hl, $7B7A: ld (down_tab + $5A), hl
    ld hl, $7D7C: ld (down_tab + $5C), hl
    ld hl, $7F7E: ld (down_tab + $5E), hl

    ld hl, $8180: ld (down_tab + $60), hl
    ld hl, $8382: ld (down_tab + $62), hl
    ld hl, $8584: ld (down_tab + $64), hl
    ld hl, $8786: ld (down_tab + $66), hl
    ld hl, $8988: ld (down_tab + $68), hl
    ld hl, $8B8A: ld (down_tab + $6A), hl
    ld hl, $8D8C: ld (down_tab + $6C), hl
    ld hl, $8F8E: ld (down_tab + $6E), hl
    ld hl, $9190: ld (down_tab + $70), hl
    ld hl, $9392: ld (down_tab + $72), hl
    ld hl, $9594: ld (down_tab + $74), hl
    ld hl, $9796: ld (down_tab + $76), hl
    ld hl, $9998: ld (down_tab + $78), hl
    ld hl, $9B9A: ld (down_tab + $7A), hl
    ld hl, $9D9C: ld (down_tab + $7C), hl
    ld hl, $9F9E: ld (down_tab + $7E), hl

    ld hl, $A1A0: ld (down_tab + $80), hl
    ld hl, $A3A2: ld (down_tab + $82), hl
    ld hl, $A5A4: ld (down_tab + $84), hl
    ld hl, $A7A6: ld (down_tab + $86), hl
    ld hl, $A9A8: ld (down_tab + $88), hl
    ld hl, $ABAA: ld (down_tab + $8A), hl
    ld hl, $ADAC: ld (down_tab + $8C), hl
    ld hl, $AFAE: ld (down_tab + $8E), hl
    ld hl, $B1B0: ld (down_tab + $90), hl
    ld hl, $B3B2: ld (down_tab + $92), hl
    ld hl, $B5B4: ld (down_tab + $94), hl
    ld hl, $B7B6: ld (down_tab + $96), hl
    ld hl, $B9B8: ld (down_tab + $98), hl
    ld hl, $BBBA: ld (down_tab + $9A), hl
    ld hl, $BDBC: ld (down_tab + $9C), hl
    ld hl, $BFBE: ld (down_tab + $9E), hl

    ld hl, $C1C0: ld (down_tab + $A0), hl
    ld hl, $C3C2: ld (down_tab + $A2), hl
    ld hl, $C5C4: ld (down_tab + $A4), hl
    ld hl, $C7C6: ld (down_tab + $A6), hl
    ld hl, $C9C8: ld (down_tab + $A8), hl
    ld hl, $CBCA: ld (down_tab + $AA), hl
    ld hl, $CDCC: ld (down_tab + $AC), hl
    ld hl, $CFCE: ld (down_tab + $AE), hl
    ld hl, $D1D0: ld (down_tab + $B0), hl
    ld hl, $D3D2: ld (down_tab + $B2), hl
    ld hl, $D5D4: ld (down_tab + $B4), hl
    ld hl, $D7D6: ld (down_tab + $B6), hl
    ld hl, $D9D8: ld (down_tab + $B8), hl
    ld hl, $DBDA: ld (down_tab + $BA), hl
    ld hl, $DDDC: ld (down_tab + $BC), hl
    ld hl, $DFDE: ld (down_tab + $BE), hl

    ld hl, $E1E0: ld (down_tab + $C0), hl
    ld hl, $E3E2: ld (down_tab + $C2), hl
    ld hl, $E5E4: ld (down_tab + $C4), hl
    ld hl, $E7E6: ld (down_tab + $C6), hl
    ld hl, $E9E8: ld (down_tab + $C8), hl
    ld hl, $EBEA: ld (down_tab + $CA), hl
    ld hl, $EDEC: ld (down_tab + $CC), hl
    ld hl, $EFEE: ld (down_tab + $CE), hl
    ld hl, $F1F0: ld (down_tab + $D0), hl
    ld hl, $F3F2: ld (down_tab + $D2), hl
    ld hl, $F5F4: ld (down_tab + $D4), hl
    ld hl, $F7F6: ld (down_tab + $D6), hl
    ld hl, $F9F8: ld (down_tab + $D8), hl
    ld hl, $FBFA: ld (down_tab + $DA), hl
    ld hl, $FDFC: ld (down_tab + $DC), hl
    ld hl, $FFFE: ld (down_tab + $DE), hl

    ld hl, $1110: ld (down_tab + $E0), hl
    ld hl, $1312: ld (down_tab + $E2), hl
    ld hl, $1514: ld (down_tab + $E4), hl
    ld hl, $1716: ld (down_tab + $E6), hl
    ld hl, $1918: ld (down_tab + $E8), hl
    ld hl, $1B1A: ld (down_tab + $EA), hl
    ld hl, $1D1C: ld (down_tab + $EC), hl
    ld hl, $1F1E: ld (down_tab + $EE), hl
    ld hl, $0100: ld (down_tab + $F0), hl
    ld hl, $0302: ld (down_tab + $F2), hl
    ld hl, $0504: ld (down_tab + $F4), hl
    ld hl, $0706: ld (down_tab + $F6), hl
    ld hl, $0908: ld (down_tab + $F8), hl
    ld hl, $0B0A: ld (down_tab + $FA), hl
    ld hl, $0D0C: ld (down_tab + $FC), hl
    ld hl, $0F0E: ld (down_tab + $FE), hl

    ; left_tab
    ld hl, $000F: ld (left_tab + $00), hl
    ld hl, $0201: ld (left_tab + $02), hl
    ld hl, $0403: ld (left_tab + $04), hl
    ld hl, $0605: ld (left_tab + $06), hl
    ld hl, $0807: ld (left_tab + $08), hl
    ld hl, $0A09: ld (left_tab + $0A), hl
    ld hl, $0C0B: ld (left_tab + $0C), hl
    ld hl, $0E0D: ld (left_tab + $0E), hl
    ld hl, $101F: ld (left_tab + $10), hl
    ld hl, $1211: ld (left_tab + $12), hl
    ld hl, $1413: ld (left_tab + $14), hl
    ld hl, $1615: ld (left_tab + $16), hl
    ld hl, $1817: ld (left_tab + $18), hl
    ld hl, $1A19: ld (left_tab + $1A), hl
    ld hl, $1C1B: ld (left_tab + $1C), hl
    ld hl, $1E1D: ld (left_tab + $1E), hl

    ld hl, $202F: ld (left_tab + $20), hl
    ld hl, $2221: ld (left_tab + $22), hl
    ld hl, $2423: ld (left_tab + $24), hl
    ld hl, $2625: ld (left_tab + $26), hl
    ld hl, $2827: ld (left_tab + $28), hl
    ld hl, $2A29: ld (left_tab + $2A), hl
    ld hl, $2C2B: ld (left_tab + $2C), hl
    ld hl, $2E2D: ld (left_tab + $2E), hl
    ld hl, $303F: ld (left_tab + $30), hl
    ld hl, $3231: ld (left_tab + $32), hl
    ld hl, $3433: ld (left_tab + $34), hl
    ld hl, $3635: ld (left_tab + $36), hl
    ld hl, $3837: ld (left_tab + $38), hl
    ld hl, $3A39: ld (left_tab + $3A), hl
    ld hl, $3C3B: ld (left_tab + $3C), hl
    ld hl, $3E3D: ld (left_tab + $3E), hl

    ld hl, $404F: ld (left_tab + $40), hl
    ld hl, $4241: ld (left_tab + $42), hl
    ld hl, $4443: ld (left_tab + $44), hl
    ld hl, $4645: ld (left_tab + $46), hl
    ld hl, $4847: ld (left_tab + $48), hl
    ld hl, $4A49: ld (left_tab + $4A), hl
    ld hl, $4C4B: ld (left_tab + $4C), hl
    ld hl, $4E4D: ld (left_tab + $4E), hl
    ld hl, $505F: ld (left_tab + $50), hl
    ld hl, $5251: ld (left_tab + $52), hl
    ld hl, $5453: ld (left_tab + $54), hl
    ld hl, $5655: ld (left_tab + $56), hl
    ld hl, $5857: ld (left_tab + $58), hl
    ld hl, $5A59: ld (left_tab + $5A), hl
    ld hl, $5C5B: ld (left_tab + $5C), hl
    ld hl, $5E5D: ld (left_tab + $5E), hl

    ld hl, $606F: ld (left_tab + $60), hl
    ld hl, $6261: ld (left_tab + $62), hl
    ld hl, $6463: ld (left_tab + $64), hl
    ld hl, $6665: ld (left_tab + $66), hl
    ld hl, $6867: ld (left_tab + $68), hl
    ld hl, $6A69: ld (left_tab + $6A), hl
    ld hl, $6C6B: ld (left_tab + $6C), hl
    ld hl, $6E6D: ld (left_tab + $6E), hl
    ld hl, $707F: ld (left_tab + $70), hl
    ld hl, $7271: ld (left_tab + $72), hl
    ld hl, $7473: ld (left_tab + $74), hl
    ld hl, $7675: ld (left_tab + $76), hl
    ld hl, $7877: ld (left_tab + $78), hl
    ld hl, $7A79: ld (left_tab + $7A), hl
    ld hl, $7C7B: ld (left_tab + $7C), hl
    ld hl, $7E7D: ld (left_tab + $7E), hl

    ld hl, $808F: ld (left_tab + $80), hl
    ld hl, $8281: ld (left_tab + $82), hl
    ld hl, $8483: ld (left_tab + $84), hl
    ld hl, $8685: ld (left_tab + $86), hl
    ld hl, $8887: ld (left_tab + $88), hl
    ld hl, $8A89: ld (left_tab + $8A), hl
    ld hl, $8C8B: ld (left_tab + $8C), hl
    ld hl, $8E8D: ld (left_tab + $8E), hl
    ld hl, $909F: ld (left_tab + $90), hl
    ld hl, $9291: ld (left_tab + $92), hl
    ld hl, $9493: ld (left_tab + $94), hl
    ld hl, $9695: ld (left_tab + $96), hl
    ld hl, $9897: ld (left_tab + $98), hl
    ld hl, $9A99: ld (left_tab + $9A), hl
    ld hl, $9C9B: ld (left_tab + $9C), hl
    ld hl, $9E9D: ld (left_tab + $9E), hl

    ld hl, $A0AF: ld (left_tab + $A0), hl
    ld hl, $A2A1: ld (left_tab + $A2), hl
    ld hl, $A4A3: ld (left_tab + $A4), hl
    ld hl, $A6A5: ld (left_tab + $A6), hl
    ld hl, $A8A7: ld (left_tab + $A8), hl
    ld hl, $AAA9: ld (left_tab + $AA), hl
    ld hl, $ACAB: ld (left_tab + $AC), hl
    ld hl, $AEAD: ld (left_tab + $AE), hl
    ld hl, $B0BF: ld (left_tab + $B0), hl
    ld hl, $B2B1: ld (left_tab + $B2), hl
    ld hl, $B4B3: ld (left_tab + $B4), hl
    ld hl, $B6B5: ld (left_tab + $B6), hl
    ld hl, $B8B7: ld (left_tab + $B8), hl
    ld hl, $BAB9: ld (left_tab + $BA), hl
    ld hl, $BCBB: ld (left_tab + $BC), hl
    ld hl, $BEBD: ld (left_tab + $BE), hl

    ld hl, $C0CF: ld (left_tab + $C0), hl
    ld hl, $C2C1: ld (left_tab + $C2), hl
    ld hl, $C4C3: ld (left_tab + $C4), hl
    ld hl, $C6C5: ld (left_tab + $C6), hl
    ld hl, $C8C7: ld (left_tab + $C8), hl
    ld hl, $CAC9: ld (left_tab + $CA), hl
    ld hl, $CCCB: ld (left_tab + $CC), hl
    ld hl, $CECD: ld (left_tab + $CE), hl
    ld hl, $D0DF: ld (left_tab + $D0), hl
    ld hl, $D2D1: ld (left_tab + $D2), hl
    ld hl, $D4D3: ld (left_tab + $D4), hl
    ld hl, $D6D5: ld (left_tab + $D6), hl
    ld hl, $D8D7: ld (left_tab + $D8), hl
    ld hl, $DAD9: ld (left_tab + $DA), hl
    ld hl, $DCDB: ld (left_tab + $DC), hl
    ld hl, $DEDD: ld (left_tab + $DE), hl

    ld hl, $E0EF: ld (left_tab + $E0), hl
    ld hl, $E2E1: ld (left_tab + $E2), hl
    ld hl, $E4E3: ld (left_tab + $E4), hl
    ld hl, $E6E5: ld (left_tab + $E6), hl
    ld hl, $E8E7: ld (left_tab + $E8), hl
    ld hl, $EAE9: ld (left_tab + $EA), hl
    ld hl, $ECEB: ld (left_tab + $EC), hl
    ld hl, $EEED: ld (left_tab + $EE), hl
    ld hl, $F0FF: ld (left_tab + $F0), hl
    ld hl, $F2F1: ld (left_tab + $F2), hl
    ld hl, $F4F3: ld (left_tab + $F4), hl
    ld hl, $F6F5: ld (left_tab + $F6), hl
    ld hl, $F8F7: ld (left_tab + $F8), hl
    ld hl, $FAF9: ld (left_tab + $FA), hl
    ld hl, $FCFB: ld (left_tab + $FC), hl
    ld hl, $FEFD: ld (left_tab + $FE), hl

    ; up_tab
    ld hl, $F1F0: ld (up_tab + $00), hl
    ld hl, $F3F2: ld (up_tab + $02), hl
    ld hl, $F5F4: ld (up_tab + $04), hl
    ld hl, $F7F6: ld (up_tab + $06), hl
    ld hl, $F9F8: ld (up_tab + $08), hl
    ld hl, $FBFA: ld (up_tab + $0A), hl
    ld hl, $FDFC: ld (up_tab + $0C), hl
    ld hl, $FFFE: ld (up_tab + $0E), hl
    ld hl, $E1E0: ld (up_tab + $10), hl
    ld hl, $E3E2: ld (up_tab + $12), hl
    ld hl, $E5E4: ld (up_tab + $14), hl
    ld hl, $E7E6: ld (up_tab + $16), hl
    ld hl, $E9E8: ld (up_tab + $18), hl
    ld hl, $EBEA: ld (up_tab + $1A), hl
    ld hl, $EDEC: ld (up_tab + $1C), hl
    ld hl, $EFEE: ld (up_tab + $1E), hl

    ld hl, $0100: ld (up_tab + $20), hl
    ld hl, $0302: ld (up_tab + $22), hl
    ld hl, $0504: ld (up_tab + $24), hl
    ld hl, $0706: ld (up_tab + $26), hl
    ld hl, $0908: ld (up_tab + $28), hl
    ld hl, $0B0A: ld (up_tab + $2A), hl
    ld hl, $0D0C: ld (up_tab + $2C), hl
    ld hl, $0F0E: ld (up_tab + $2E), hl
    ld hl, $1110: ld (up_tab + $30), hl
    ld hl, $1312: ld (up_tab + $32), hl
    ld hl, $1514: ld (up_tab + $34), hl
    ld hl, $1716: ld (up_tab + $36), hl
    ld hl, $1918: ld (up_tab + $38), hl
    ld hl, $1B1A: ld (up_tab + $3A), hl
    ld hl, $1D1C: ld (up_tab + $3C), hl
    ld hl, $1F1E: ld (up_tab + $3E), hl

    ld hl, $2120: ld (up_tab + $40), hl
    ld hl, $2322: ld (up_tab + $42), hl
    ld hl, $2524: ld (up_tab + $44), hl
    ld hl, $2726: ld (up_tab + $46), hl
    ld hl, $2928: ld (up_tab + $48), hl
    ld hl, $2B2A: ld (up_tab + $4A), hl
    ld hl, $2D2C: ld (up_tab + $4C), hl
    ld hl, $2F2E: ld (up_tab + $4E), hl
    ld hl, $3130: ld (up_tab + $50), hl
    ld hl, $3332: ld (up_tab + $52), hl
    ld hl, $3534: ld (up_tab + $54), hl
    ld hl, $3736: ld (up_tab + $56), hl
    ld hl, $3938: ld (up_tab + $58), hl
    ld hl, $3B3A: ld (up_tab + $5A), hl
    ld hl, $3D3C: ld (up_tab + $5C), hl
    ld hl, $3F3E: ld (up_tab + $5E), hl

    ld hl, $4140: ld (up_tab + $60), hl
    ld hl, $4342: ld (up_tab + $62), hl
    ld hl, $4544: ld (up_tab + $64), hl
    ld hl, $4746: ld (up_tab + $66), hl
    ld hl, $4948: ld (up_tab + $68), hl
    ld hl, $4B4A: ld (up_tab + $6A), hl
    ld hl, $4D4C: ld (up_tab + $6C), hl
    ld hl, $4F4E: ld (up_tab + $6E), hl
    ld hl, $5150: ld (up_tab + $70), hl
    ld hl, $5352: ld (up_tab + $72), hl
    ld hl, $5554: ld (up_tab + $74), hl
    ld hl, $5756: ld (up_tab + $76), hl
    ld hl, $5958: ld (up_tab + $78), hl
    ld hl, $5B5A: ld (up_tab + $7A), hl
    ld hl, $5D5C: ld (up_tab + $7C), hl
    ld hl, $5F5E: ld (up_tab + $7E), hl

    ld hl, $6160: ld (up_tab + $80), hl
    ld hl, $6362: ld (up_tab + $82), hl
    ld hl, $6564: ld (up_tab + $84), hl
    ld hl, $6766: ld (up_tab + $86), hl
    ld hl, $6968: ld (up_tab + $88), hl
    ld hl, $6B6A: ld (up_tab + $8A), hl
    ld hl, $6D6C: ld (up_tab + $8C), hl
    ld hl, $6F6E: ld (up_tab + $8E), hl
    ld hl, $7170: ld (up_tab + $90), hl
    ld hl, $7372: ld (up_tab + $92), hl
    ld hl, $7574: ld (up_tab + $94), hl
    ld hl, $7776: ld (up_tab + $96), hl
    ld hl, $7978: ld (up_tab + $98), hl
    ld hl, $7B7A: ld (up_tab + $9A), hl
    ld hl, $7D7C: ld (up_tab + $9C), hl
    ld hl, $7F7E: ld (up_tab + $9E), hl

    ld hl, $8180: ld (up_tab + $A0), hl
    ld hl, $8382: ld (up_tab + $A2), hl
    ld hl, $8584: ld (up_tab + $A4), hl
    ld hl, $8786: ld (up_tab + $A6), hl
    ld hl, $8988: ld (up_tab + $A8), hl
    ld hl, $8B8A: ld (up_tab + $AA), hl
    ld hl, $8D8C: ld (up_tab + $AC), hl
    ld hl, $8F8E: ld (up_tab + $AE), hl
    ld hl, $9190: ld (up_tab + $B0), hl
    ld hl, $9392: ld (up_tab + $B2), hl
    ld hl, $9594: ld (up_tab + $B4), hl
    ld hl, $9796: ld (up_tab + $B6), hl
    ld hl, $9998: ld (up_tab + $B8), hl
    ld hl, $9B9A: ld (up_tab + $BA), hl
    ld hl, $9D9C: ld (up_tab + $BC), hl
    ld hl, $9F9E: ld (up_tab + $BE), hl

    ld hl, $A1A0: ld (up_tab + $C0), hl
    ld hl, $A3A2: ld (up_tab + $C2), hl
    ld hl, $A5A4: ld (up_tab + $C4), hl
    ld hl, $A7A6: ld (up_tab + $C6), hl
    ld hl, $A9A8: ld (up_tab + $C8), hl
    ld hl, $ABAA: ld (up_tab + $CA), hl
    ld hl, $ADAC: ld (up_tab + $CC), hl
    ld hl, $AFAE: ld (up_tab + $CE), hl
    ld hl, $B1B0: ld (up_tab + $D0), hl
    ld hl, $B3B2: ld (up_tab + $D2), hl
    ld hl, $B5B4: ld (up_tab + $D4), hl
    ld hl, $B7B6: ld (up_tab + $D6), hl
    ld hl, $B9B8: ld (up_tab + $D8), hl
    ld hl, $BBBA: ld (up_tab + $DA), hl
    ld hl, $BDBC: ld (up_tab + $DC), hl
    ld hl, $BFBE: ld (up_tab + $DE), hl

    ld hl, $C1C0: ld (up_tab + $E0), hl
    ld hl, $C3C2: ld (up_tab + $E2), hl
    ld hl, $C5C4: ld (up_tab + $E4), hl
    ld hl, $C7C6: ld (up_tab + $E6), hl
    ld hl, $C9C8: ld (up_tab + $E8), hl
    ld hl, $CBCA: ld (up_tab + $EA), hl
    ld hl, $CDCC: ld (up_tab + $EC), hl
    ld hl, $CFCE: ld (up_tab + $EE), hl
    ld hl, $D1D0: ld (up_tab + $F0), hl
    ld hl, $D3D2: ld (up_tab + $F2), hl
    ld hl, $D5D4: ld (up_tab + $F4), hl
    ld hl, $D7D6: ld (up_tab + $F6), hl
    ld hl, $D9D8: ld (up_tab + $F8), hl
    ld hl, $DBDA: ld (up_tab + $FA), hl
    ld hl, $DDDC: ld (up_tab + $FC), hl
    ld hl, $DFDE: ld (up_tab + $FE), hl

    ; key_tab
    ld hl, no_key * 257                             ; NONE  NONE
    ld bc, (HIGH left_tab) + (HIGH right_tab) * 256        ; LEFT  RIGHT
    ld de, no_key + (HIGH left_tab)  * 256          ; NONE  LEFT
    ld ix, (HIGH down_tab) + (HIGH up_tab) * 256    ; DOWN  UP


    ld (key_tab), hl
    ld (key_tab + $02), hl
    ld (key_tab + $04), hl
    ld (key_tab + $06), hl
    ld (key_tab + $08), bc ; LEFT  RIGHT
    ld (key_tab + $0A), ix ; DOWN  UP

    ld bc, (HIGH right_tab) + (HIGH no_key) * 256   ; RIGHT NONE

    ld (key_tab + $0C), hl
    ld (key_tab + $0E), hl
    ld (key_tab + $10), hl
    ld (key_tab + $12), hl
    ld (key_tab + $14), hl
    ld (key_tab + $16), hl
    ld (key_tab + $18), hl
    ld (key_tab + $1A), hl
    ld (key_tab + $1C), hl
    ld (key_tab + $1E), hl
    ld (key_tab + $20), hl ;  !
    ld (key_tab + $22), hl ; "#
    ld (key_tab + $24), hl ; $%
    ld (key_tab + $26), hl ; &'
    ld (key_tab + $28), hl ; ()
    ld (key_tab + $2A), hl ; *+
    ld (key_tab + $2C), hl ; ,-
    ld (key_tab + $2E), hl ; ./
    ld (key_tab + $30), hl ; 01
    ld (key_tab + $32), hl ; 23
    ld (key_tab + $34), de ; 45 ; NONE  LEFT
    ld (key_tab + $36), ix ; 67 ; DOWN  UP
    ld (key_tab + $38), bc ; 89 ; RIGHT NONE

    ld bc, no_key + (HIGH down_tab)  * 256          ; NONE  DOWN

    ld (key_tab + $3A), hl ; :;
    ld (key_tab + $3C), hl ; <=
    ld (key_tab + $3E), hl ; >?
    ld (key_tab + $40), bc ; @A ; NONE  DOWN

    ld bc, (HIGH right_tab) + (HIGH no_key) * 256   ; RIGHT NONE
    ld ix, (HIGH down_tab) + (HIGH up_tab) * 256    ; DOWN  UP

    ld (key_tab + $42), hl ; BC
    ld (key_tab + $44), ix ; DE ; DOWN  UP
    ld (key_tab + $46), bc ; FG ; RIGHT NONE
    ld (key_tab + $48), hl ; HI
    ld (key_tab + $4A), hl ; JK
    ld (key_tab + $4C), hl ; LM
    ld (key_tab + $4E), de ; NO ; NONE  LEFT

    ld ix, (HIGH right_tab) + (HIGH up_tab) * 256   ; RIGHT UP
    ld bc, no_key + (HIGH down_tab)  * 256          ; NONE  DOWN

    ld (key_tab + $50), ix ; PQ ; RIGHT UP
    ld (key_tab + $52), de ; RS ; NONE  LEFT
    ld (key_tab + $54), hl ; TU
    ld (key_tab + $56), hl ; VW
    ld (key_tab + $58), hl ; XY
    ld (key_tab + $5A), hl ; Z[
    ld (key_tab + $5C), hl ; \]
    ld (key_tab + $5E), hl ; ^_
    ld (key_tab + $60), bc ; $a ; NONE  DOWN

    ld bc, (HIGH right_tab) + (HIGH no_key) * 256   ; RIGHT NONE
    ld ix, (HIGH down_tab) + (HIGH up_tab) * 256    ; DOWN  UP

    ld (key_tab + $62), hl ; bc
    ld (key_tab + $64), ix ; de ; DOWN  UP
    ld (key_tab + $66), bc ; fg ; RIGHT NONE
    ld (key_tab + $68), hl ; hi
    ld (key_tab + $6A), hl ; jk
    ld (key_tab + $6C), hl ; lm
    ld (key_tab + $6E), de ; no ; NONE  LEFT

    ld ix, (HIGH right_tab) + (HIGH up_tab) * 256   ; RIGHT UP

    ld (key_tab + $70), ix ; pq ; RIGHT UP
    ld (key_tab + $72), de ; rs ; NONE  LEFT
    ld (key_tab + $74), hl ; tu
    ld (key_tab + $76), hl ; vw
    ld (key_tab + $78), hl ; xy
    ld (key_tab + $7A), hl ; z{
    ld (key_tab + $7C), hl ; |}
    ld (key_tab + $7E), hl ; ~C
    ld (key_tab + $80), hl
    ld (key_tab + $82), hl
    ld (key_tab + $84), hl
    ld (key_tab + $86), hl
    ld (key_tab + $88), hl
    ld (key_tab + $8A), hl
    ld (key_tab + $8C), hl
    ld (key_tab + $8E), hl
    ld (key_tab + $90), hl ; AB
    ld (key_tab + $92), hl ; CD
    ld (key_tab + $94), hl ; EF
    ld (key_tab + $96), hl ; GH
    ld (key_tab + $98), hl ; IJ
    ld (key_tab + $9A), hl ; KL
    ld (key_tab + $9C), hl ; MN
    ld (key_tab + $9E), hl ; OP
    ld (key_tab + $A0), hl ; QR
    ld (key_tab + $A2), hl ; ST
    ld (key_tab + $A4), hl ; U RND
    ld (key_tab + $A6), hl ; INKEY$ PI
    ld (key_tab + $A8), hl ; FN POINT
    ld (key_tab + $AA), hl ; SCREEN$ ATTR
    ld (key_tab + $AC), hl ; AT TAB
    ld (key_tab + $AE), hl ; VAL$ CODE
    ld (key_tab + $B0), hl ; VAL LEN
    ld (key_tab + $B2), hl ; SIN COS
    ld (key_tab + $B4), hl ; TAN ASN
    ld (key_tab + $B6), hl ; ACS ATN
    ld (key_tab + $B8), hl ; LN EXP
    ld (key_tab + $BA), hl ; INT SQR
    ld (key_tab + $BC), hl ; SGN ABS
    ld (key_tab + $BE), hl ; PEEK IN
    ld (key_tab + $C0), hl ; USR STR$
    ld (key_tab + $C2), hl ; CHR$ NOT
    ld (key_tab + $C4), hl ; BIN OR
    ld (key_tab + $C6), hl ; AND <=
    ld (key_tab + $C8), hl ; >= <>
    ld (key_tab + $CA), hl ; LINE THEN
    ld (key_tab + $CC), hl ; TO STEP
    ld (key_tab + $CE), hl ; DEF FN CAT
    ld (key_tab + $D0), hl ; FORMAT MOVE
    ld (key_tab + $D2), hl ; ERASE OPEN #
    ld (key_tab + $D4), hl ; CLOSE # MERGE
    ld (key_tab + $D6), hl ; VERIFY BEEP
    ld (key_tab + $D8), hl ; CIRCLE INK
    ld (key_tab + $DA), hl ; PAPER FLASH
    ld (key_tab + $DC), hl ; BRIGHT INVERSE
    ld (key_tab + $DE), hl ; OVER OUT
    ld (key_tab + $E0), hl ; LPRINT LLIST
    ld (key_tab + $E2), hl ; STOP READ
    ld (key_tab + $E4), hl ; DATA RESTORE
    ld (key_tab + $E6), hl ; NEW BORDER
    ld (key_tab + $E8), hl ; CONTINUE DIM
    ld (key_tab + $EA), hl ; REM FOR
    ld (key_tab + $EC), hl ; GO TO GO SUB
    ld (key_tab + $EE), hl ; INPUT LOAD
    ld (key_tab + $F0), hl ; LIST LET
    ld (key_tab + $F2), hl ; PAUSE NEXT
    ld (key_tab + $F4), hl ; POKE PRINT
    ld (key_tab + $F6), hl ; PLOT RUN
    ld (key_tab + $F8), hl ; SAVE RANDOMIZE
    ld (key_tab + $FA), hl ; IF CLS
    ld (key_tab + $FC), hl ; DRAW CLEAR
    ld (key_tab + $FE), hl ; RETURN COPY

    ; opposite_dir_tab
    ld a, HIGH left_tab
    ld (opposite_dir_tab + (HIGH right_tab)), a
    ld a, HIGH up_tab
    ld (opposite_dir_tab + (HIGH down_tab)), a
    ld a, HIGH right_tab
    ld (opposite_dir_tab + (HIGH left_tab)), a
    ld a, HIGH down_tab
    ld (opposite_dir_tab + (HIGH up_tab)), a

    ; inc_tab
    ld hl, $0201: ld (inc_tab), hl
    ld hl, $0403: ld (inc_tab + $02), hl
    ld hl, $0605: ld (inc_tab + $04), hl
    ld hl, $0807: ld (inc_tab + $06), hl
    ld hl, $0A09: ld (inc_tab + $08), hl
    ld hl, $0C0B: ld (inc_tab + $0A), hl
    ld hl, $0E0D: ld (inc_tab + $0C), hl
    ld hl, $100F: ld (inc_tab + $0E), hl
    ld hl, $1211: ld (inc_tab + $10), hl
    ld hl, $1413: ld (inc_tab + $12), hl
    ld hl, $1615: ld (inc_tab + $14), hl
    ld hl, $1817: ld (inc_tab + $16), hl
    ld hl, $1A19: ld (inc_tab + $18), hl
    ld hl, $1C1B: ld (inc_tab + $1A), hl
    ld hl, $1E1D: ld (inc_tab + $1C), hl
    ld hl, $201F: ld (inc_tab + $1E), hl

    ld hl, $2221: ld (inc_tab + $20), hl
    ld hl, $2423: ld (inc_tab + $22), hl
    ld hl, $2625: ld (inc_tab + $24), hl
    ld hl, $2827: ld (inc_tab + $26), hl
    ld hl, $2A29: ld (inc_tab + $28), hl
    ld hl, $2C2B: ld (inc_tab + $2A), hl
    ld hl, $2E2D: ld (inc_tab + $2C), hl
    ld hl, $302F: ld (inc_tab + $2E), hl
    ld hl, $3231: ld (inc_tab + $30), hl
    ld hl, $3433: ld (inc_tab + $32), hl
    ld hl, $3635: ld (inc_tab + $34), hl
    ld hl, $3837: ld (inc_tab + $36), hl
    ld hl, $3A39: ld (inc_tab + $38), hl
    ld hl, $3C3B: ld (inc_tab + $3A), hl
    ld hl, $3E3D: ld (inc_tab + $3C), hl
    ld hl, $403F: ld (inc_tab + $3E), hl

    ld hl, $4241: ld (inc_tab + $40), hl
    ld hl, $4443: ld (inc_tab + $42), hl
    ld hl, $4645: ld (inc_tab + $44), hl
    ld hl, $4847: ld (inc_tab + $46), hl
    ld hl, $4A49: ld (inc_tab + $48), hl
    ld hl, $4C4B: ld (inc_tab + $4A), hl
    ld hl, $4E4D: ld (inc_tab + $4C), hl
    ld hl, $504F: ld (inc_tab + $4E), hl
    ld hl, $5251: ld (inc_tab + $50), hl
    ld hl, $5453: ld (inc_tab + $52), hl
    ld hl, $5655: ld (inc_tab + $54), hl
    ld hl, $5857: ld (inc_tab + $56), hl
    ld hl, $5A59: ld (inc_tab + $58), hl
    ld hl, $5C5B: ld (inc_tab + $5A), hl
    ld hl, $5E5D: ld (inc_tab + $5C), hl
    ld hl, $605F: ld (inc_tab + $5E), hl

    ld hl, $6261: ld (inc_tab + $60), hl
    ld hl, $6463: ld (inc_tab + $62), hl
    ld hl, $6665: ld (inc_tab + $64), hl
    ld hl, $6867: ld (inc_tab + $66), hl
    ld hl, $6A69: ld (inc_tab + $68), hl
    ld hl, $6C6B: ld (inc_tab + $6A), hl
    ld hl, $6E6D: ld (inc_tab + $6C), hl
    ld hl, $706F: ld (inc_tab + $6E), hl
    ld hl, $7271: ld (inc_tab + $70), hl
    ld hl, $7473: ld (inc_tab + $72), hl
    ld hl, $7675: ld (inc_tab + $74), hl
    ld hl, $7877: ld (inc_tab + $76), hl
    ld hl, $7A79: ld (inc_tab + $78), hl
    ld hl, $7C7B: ld (inc_tab + $7A), hl
    ld hl, $7E7D: ld (inc_tab + $7C), hl
    ld hl, $807F: ld (inc_tab + $7E), hl

    ld hl, $8281: ld (inc_tab + $80), hl
    ld hl, $8483: ld (inc_tab + $82), hl
    ld hl, $8685: ld (inc_tab + $84), hl
    ld hl, $8887: ld (inc_tab + $86), hl
    ld hl, $8A89: ld (inc_tab + $88), hl
    ld hl, $8C8B: ld (inc_tab + $8A), hl
    ld hl, $8E8D: ld (inc_tab + $8C), hl
    ld hl, $908F: ld (inc_tab + $8E), hl
    ld hl, $9291: ld (inc_tab + $90), hl
    ld hl, $9493: ld (inc_tab + $92), hl
    ld hl, $9695: ld (inc_tab + $94), hl
    ld hl, $9897: ld (inc_tab + $96), hl
    ld hl, $9A99: ld (inc_tab + $98), hl
    ld hl, $9C9B: ld (inc_tab + $9A), hl
    ld hl, $9E9D: ld (inc_tab + $9C), hl
    ld hl, $A09F: ld (inc_tab + $9E), hl

    ld hl, $A2A1: ld (inc_tab + $A0), hl
    ld hl, $A4A3: ld (inc_tab + $A2), hl
    ld hl, $A6A5: ld (inc_tab + $A4), hl
    ld hl, $A8A7: ld (inc_tab + $A6), hl
    ld hl, $AAA9: ld (inc_tab + $A8), hl
    ld hl, $ACAB: ld (inc_tab + $AA), hl
    ld hl, $AEAD: ld (inc_tab + $AC), hl
    ld hl, $B0AF: ld (inc_tab + $AE), hl
    ld hl, $B2B1: ld (inc_tab + $B0), hl
    ld hl, $B4B3: ld (inc_tab + $B2), hl
    ld hl, $B6B5: ld (inc_tab + $B4), hl
    ld hl, $B8B7: ld (inc_tab + $B6), hl
    ld hl, $BAB9: ld (inc_tab + $B8), hl
    ld hl, $BCBB: ld (inc_tab + $BA), hl
    ld hl, $BEBD: ld (inc_tab + $BC), hl
    ld hl, $C0BF: ld (inc_tab + $BE), hl

    ld hl, $C2C1: ld (inc_tab + $C0), hl
    ld hl, $C4C3: ld (inc_tab + $C2), hl
    ld hl, $C6C5: ld (inc_tab + $C4), hl
    ld hl, $C8C7: ld (inc_tab + $C6), hl
    ld hl, $CAC9: ld (inc_tab + $C8), hl
    ld hl, $CCCB: ld (inc_tab + $CA), hl
    ld hl, $CECD: ld (inc_tab + $CC), hl
    ld hl, $D0CF: ld (inc_tab + $CE), hl
    ld hl, $D2D1: ld (inc_tab + $D0), hl
    ld hl, $D4D3: ld (inc_tab + $D2), hl
    ld hl, $D6D5: ld (inc_tab + $D4), hl
    ld hl, $D8D7: ld (inc_tab + $D6), hl
    ld hl, $DAD9: ld (inc_tab + $D8), hl
    ld hl, $DCDB: ld (inc_tab + $DA), hl
    ld hl, $DEDD: ld (inc_tab + $DC), hl
    ld hl, $E0DF: ld (inc_tab + $DE), hl

    ld hl, $E2E1: ld (inc_tab + $E0), hl
    ld hl, $E4E3: ld (inc_tab + $E2), hl
    ld hl, $E6E5: ld (inc_tab + $E4), hl
    ld hl, $E8E7: ld (inc_tab + $E6), hl
    ld hl, $EAE9: ld (inc_tab + $E8), hl
    ld hl, $ECEB: ld (inc_tab + $EA), hl
    ld hl, $EEED: ld (inc_tab + $EC), hl
    ld hl, $F0EF: ld (inc_tab + $EE), hl
    ld hl, $F2F1: ld (inc_tab + $F0), hl
    ld hl, $F4F3: ld (inc_tab + $F2), hl
    ld hl, $F6F5: ld (inc_tab + $F4), hl
    ld hl, $F8F7: ld (inc_tab + $F6), hl
    ld hl, $FAF9: ld (inc_tab + $F8), hl
    ld hl, $FCFB: ld (inc_tab + $FA), hl
    ld hl, $FEFD: ld (inc_tab + $FC), hl
    ld hl, $00FF: ld (inc_tab + $FE), hl

    ; dec_tab
    ld hl, $00FF: ld (dec_tab), hl
    ld hl, $0201: ld (dec_tab + $02), hl
    ld hl, $0403: ld (dec_tab + $04), hl
    ld hl, $0605: ld (dec_tab + $06), hl
    ld hl, $0807: ld (dec_tab + $08), hl
    ld hl, $0A09: ld (dec_tab + $0A), hl
    ld hl, $0C0B: ld (dec_tab + $0C), hl
    ld hl, $0E0D: ld (dec_tab + $0E), hl
    ld hl, $100F: ld (dec_tab + $10), hl
    ld hl, $1211: ld (dec_tab + $12), hl
    ld hl, $1413: ld (dec_tab + $14), hl
    ld hl, $1615: ld (dec_tab + $16), hl
    ld hl, $1817: ld (dec_tab + $18), hl
    ld hl, $1A19: ld (dec_tab + $1A), hl
    ld hl, $1C1B: ld (dec_tab + $1C), hl
    ld hl, $1E1D: ld (dec_tab + $1E), hl

    ld hl, $201F: ld (dec_tab + $20), hl
    ld hl, $2221: ld (dec_tab + $22), hl
    ld hl, $2423: ld (dec_tab + $24), hl
    ld hl, $2625: ld (dec_tab + $26), hl
    ld hl, $2827: ld (dec_tab + $28), hl
    ld hl, $2A29: ld (dec_tab + $2A), hl
    ld hl, $2C2B: ld (dec_tab + $2C), hl
    ld hl, $2E2D: ld (dec_tab + $2E), hl
    ld hl, $302F: ld (dec_tab + $30), hl
    ld hl, $3231: ld (dec_tab + $32), hl
    ld hl, $3433: ld (dec_tab + $34), hl
    ld hl, $3635: ld (dec_tab + $36), hl
    ld hl, $3837: ld (dec_tab + $38), hl
    ld hl, $3A39: ld (dec_tab + $3A), hl
    ld hl, $3C3B: ld (dec_tab + $3C), hl
    ld hl, $3E3D: ld (dec_tab + $3E), hl

    ld hl, $403F: ld (dec_tab + $40), hl
    ld hl, $4241: ld (dec_tab + $42), hl
    ld hl, $4443: ld (dec_tab + $44), hl
    ld hl, $4645: ld (dec_tab + $46), hl
    ld hl, $4847: ld (dec_tab + $48), hl
    ld hl, $4A49: ld (dec_tab + $4A), hl
    ld hl, $4C4B: ld (dec_tab + $4C), hl
    ld hl, $4E4D: ld (dec_tab + $4E), hl
    ld hl, $504F: ld (dec_tab + $50), hl
    ld hl, $5251: ld (dec_tab + $52), hl
    ld hl, $5453: ld (dec_tab + $54), hl
    ld hl, $5655: ld (dec_tab + $56), hl
    ld hl, $5857: ld (dec_tab + $58), hl
    ld hl, $5A59: ld (dec_tab + $5A), hl
    ld hl, $5C5B: ld (dec_tab + $5C), hl
    ld hl, $5E5D: ld (dec_tab + $5E), hl

    ld hl, $605F: ld (dec_tab + $60), hl
    ld hl, $6261: ld (dec_tab + $62), hl
    ld hl, $6463: ld (dec_tab + $64), hl
    ld hl, $6665: ld (dec_tab + $66), hl
    ld hl, $6867: ld (dec_tab + $68), hl
    ld hl, $6A69: ld (dec_tab + $6A), hl
    ld hl, $6C6B: ld (dec_tab + $6C), hl
    ld hl, $6E6D: ld (dec_tab + $6E), hl
    ld hl, $706F: ld (dec_tab + $70), hl
    ld hl, $7271: ld (dec_tab + $72), hl
    ld hl, $7473: ld (dec_tab + $74), hl
    ld hl, $7675: ld (dec_tab + $76), hl
    ld hl, $7877: ld (dec_tab + $78), hl
    ld hl, $7A79: ld (dec_tab + $7A), hl
    ld hl, $7C7B: ld (dec_tab + $7C), hl
    ld hl, $7E7D: ld (dec_tab + $7E), hl

    ld hl, $807F: ld (dec_tab + $80), hl
    ld hl, $8281: ld (dec_tab + $82), hl
    ld hl, $8483: ld (dec_tab + $84), hl
    ld hl, $8685: ld (dec_tab + $86), hl
    ld hl, $8887: ld (dec_tab + $88), hl
    ld hl, $8A89: ld (dec_tab + $8A), hl
    ld hl, $8C8B: ld (dec_tab + $8C), hl
    ld hl, $8E8D: ld (dec_tab + $8E), hl
    ld hl, $908F: ld (dec_tab + $90), hl
    ld hl, $9291: ld (dec_tab + $92), hl
    ld hl, $9493: ld (dec_tab + $94), hl
    ld hl, $9695: ld (dec_tab + $96), hl
    ld hl, $9897: ld (dec_tab + $98), hl
    ld hl, $9A99: ld (dec_tab + $9A), hl
    ld hl, $9C9B: ld (dec_tab + $9C), hl
    ld hl, $9E9D: ld (dec_tab + $9E), hl

    ld hl, $A09F: ld (dec_tab + $A0), hl
    ld hl, $A2A1: ld (dec_tab + $A2), hl
    ld hl, $A4A3: ld (dec_tab + $A4), hl
    ld hl, $A6A5: ld (dec_tab + $A6), hl
    ld hl, $A8A7: ld (dec_tab + $A8), hl
    ld hl, $AAA9: ld (dec_tab + $AA), hl
    ld hl, $ACAB: ld (dec_tab + $AC), hl
    ld hl, $AEAD: ld (dec_tab + $AE), hl
    ld hl, $B0AF: ld (dec_tab + $B0), hl
    ld hl, $B2B1: ld (dec_tab + $B2), hl
    ld hl, $B4B3: ld (dec_tab + $B4), hl
    ld hl, $B6B5: ld (dec_tab + $B6), hl
    ld hl, $B8B7: ld (dec_tab + $B8), hl
    ld hl, $BAB9: ld (dec_tab + $BA), hl
    ld hl, $BCBB: ld (dec_tab + $BC), hl
    ld hl, $BEBD: ld (dec_tab + $BE), hl

    ld hl, $C0BF: ld (dec_tab + $C0), hl
    ld hl, $C2C1: ld (dec_tab + $C2), hl
    ld hl, $C4C3: ld (dec_tab + $C4), hl
    ld hl, $C6C5: ld (dec_tab + $C6), hl
    ld hl, $C8C7: ld (dec_tab + $C8), hl
    ld hl, $CAC9: ld (dec_tab + $CA), hl
    ld hl, $CCCB: ld (dec_tab + $CC), hl
    ld hl, $CECD: ld (dec_tab + $CE), hl
    ld hl, $D0CF: ld (dec_tab + $D0), hl
    ld hl, $D2D1: ld (dec_tab + $D2), hl
    ld hl, $D4D3: ld (dec_tab + $D4), hl
    ld hl, $D6D5: ld (dec_tab + $D6), hl
    ld hl, $D8D7: ld (dec_tab + $D8), hl
    ld hl, $DAD9: ld (dec_tab + $DA), hl
    ld hl, $DCDB: ld (dec_tab + $DC), hl
    ld hl, $DEDD: ld (dec_tab + $DE), hl

    ld hl, $E0DF: ld (dec_tab + $E0), hl
    ld hl, $E2E1: ld (dec_tab + $E2), hl
    ld hl, $E4E3: ld (dec_tab + $E4), hl
    ld hl, $E6E5: ld (dec_tab + $E6), hl
    ld hl, $E8E7: ld (dec_tab + $E8), hl
    ld hl, $EAE9: ld (dec_tab + $EA), hl
    ld hl, $ECEB: ld (dec_tab + $EC), hl
    ld hl, $EEED: ld (dec_tab + $EE), hl
    ld hl, $F0EF: ld (dec_tab + $F0), hl
    ld hl, $F2F1: ld (dec_tab + $F2), hl
    ld hl, $F4F3: ld (dec_tab + $F4), hl
    ld hl, $F6F5: ld (dec_tab + $F6), hl
    ld hl, $F8F7: ld (dec_tab + $F8), hl
    ld hl, $FAF9: ld (dec_tab + $FA), hl
    ld hl, $FCFB: ld (dec_tab + $FC), hl
    ld hl, $FEFD: ld (dec_tab + $FE), hl

    ; digit_tab: most significant digit
    ld hl, font_sp * 257

    ld (digit_tab), hl
    ld (digit_tab + $02), hl
    ld (digit_tab + $04), hl
    ld (digit_tab + $06), hl
    ld (digit_tab + $08), hl
    ld (digit_tab + $0A), hl
    ld (digit_tab + $0C), hl
    ld (digit_tab + $0E), hl
    ld (digit_tab + $10), hl
    ld (digit_tab + $12), hl
    ld (digit_tab + $14), hl
    ld (digit_tab + $16), hl
    ld (digit_tab + $18), hl
    ld (digit_tab + $1A), hl
    ld (digit_tab + $1C), hl
    ld (digit_tab + $1E), hl

    ld (digit_tab + $20), hl
    ld (digit_tab + $22), hl
    ld (digit_tab + $24), hl
    ld (digit_tab + $26), hl
    ld (digit_tab + $28), hl
    ld (digit_tab + $2A), hl
    ld (digit_tab + $2C), hl
    ld (digit_tab + $2E), hl
    ld (digit_tab + $30), hl
    ld (digit_tab + $32), hl
    ld (digit_tab + $34), hl
    ld (digit_tab + $36), hl
    ld (digit_tab + $38), hl
    ld (digit_tab + $3A), hl
    ld (digit_tab + $3C), hl
    ld (digit_tab + $3E), hl

    ld (digit_tab + $40), hl
    ld (digit_tab + $42), hl
    ld (digit_tab + $44), hl
    ld (digit_tab + $46), hl
    ld (digit_tab + $48), hl
    ld (digit_tab + $4A), hl
    ld (digit_tab + $4C), hl
    ld (digit_tab + $4E), hl
    ld (digit_tab + $50), hl
    ld (digit_tab + $52), hl
    ld (digit_tab + $54), hl
    ld (digit_tab + $56), hl
    ld (digit_tab + $58), hl
    ld (digit_tab + $5A), hl
    ld (digit_tab + $5C), hl
    ld (digit_tab + $5E), hl

    ld (digit_tab + $60), hl
    ld (digit_tab + $62), hl

    ld hl, font_1 * 257

    ld (digit_tab + $64), hl
    ld (digit_tab + $66), hl
    ld (digit_tab + $68), hl
    ld (digit_tab + $6A), hl
    ld (digit_tab + $6C), hl
    ld (digit_tab + $6E), hl
    ld (digit_tab + $70), hl
    ld (digit_tab + $72), hl
    ld (digit_tab + $74), hl
    ld (digit_tab + $76), hl
    ld (digit_tab + $78), hl
    ld (digit_tab + $7A), hl
    ld (digit_tab + $7C), hl
    ld (digit_tab + $7E), hl

    ld (digit_tab + $80), hl
    ld (digit_tab + $82), hl
    ld (digit_tab + $84), hl
    ld (digit_tab + $86), hl
    ld (digit_tab + $88), hl
    ld (digit_tab + $8A), hl
    ld (digit_tab + $8C), hl
    ld (digit_tab + $8E), hl
    ld (digit_tab + $90), hl
    ld (digit_tab + $92), hl
    ld (digit_tab + $94), hl
    ld (digit_tab + $96), hl
    ld (digit_tab + $98), hl
    ld (digit_tab + $9A), hl
    ld (digit_tab + $9C), hl
    ld (digit_tab + $9E), hl

    ld (digit_tab + $A0), hl
    ld (digit_tab + $A2), hl
    ld (digit_tab + $A4), hl
    ld (digit_tab + $A6), hl
    ld (digit_tab + $A8), hl
    ld (digit_tab + $AA), hl
    ld (digit_tab + $AC), hl
    ld (digit_tab + $AE), hl
    ld (digit_tab + $B0), hl
    ld (digit_tab + $B2), hl
    ld (digit_tab + $B4), hl
    ld (digit_tab + $B6), hl
    ld (digit_tab + $B8), hl
    ld (digit_tab + $BA), hl
    ld (digit_tab + $BC), hl
    ld (digit_tab + $BE), hl

    ld (digit_tab + $C0), hl
    ld (digit_tab + $C2), hl
    ld (digit_tab + $C4), hl
    ld (digit_tab + $C6), hl

    ld hl, font_2 * 257

    ld (digit_tab + $C8), hl
    ld (digit_tab + $CA), hl
    ld (digit_tab + $CC), hl
    ld (digit_tab + $CE), hl
    ld (digit_tab + $D0), hl
    ld (digit_tab + $D2), hl
    ld (digit_tab + $D4), hl
    ld (digit_tab + $D6), hl
    ld (digit_tab + $D8), hl
    ld (digit_tab + $DA), hl
    ld (digit_tab + $DC), hl
    ld (digit_tab + $DE), hl

    ld (digit_tab + $E0), hl
    ld (digit_tab + $E2), hl
    ld (digit_tab + $E4), hl
    ld (digit_tab + $E6), hl
    ld (digit_tab + $E8), hl
    ld (digit_tab + $EA), hl
    ld (digit_tab + $EC), hl
    ld (digit_tab + $EE), hl
    ld (digit_tab + $F0), hl
    ld (digit_tab + $F2), hl
    ld (digit_tab + $F4), hl
    ld (digit_tab + $F6), hl
    ld (digit_tab + $F8), hl
    ld (digit_tab + $FA), hl
    ld (digit_tab + $FC), hl
    ld (digit_tab + $FE), hl

    ; digit_tab: medium significant digit
    ld hl, font_sp * 257
    ld (digit_tab + $100), hl
    ld (digit_tab + $102), hl
    ld (digit_tab + $104), hl
    ld (digit_tab + $106), hl
    ld (digit_tab + $108), hl

    ld hl, font_1 * 257
    ld (digit_tab + $10A), hl
    ld (digit_tab + $10C), hl
    ld (digit_tab + $10E), hl
    ld (digit_tab + $110), hl
    ld (digit_tab + $112), hl

    ld hl, font_2 * 257
    ld (digit_tab + $114), hl
    ld (digit_tab + $116), hl
    ld (digit_tab + $118), hl
    ld (digit_tab + $11A), hl
    ld (digit_tab + $11C), hl

    ld hl, font_3 * 257
    ld (digit_tab + $11E), hl
    ld (digit_tab + $120), hl
    ld (digit_tab + $122), hl
    ld (digit_tab + $124), hl
    ld (digit_tab + $126), hl

    ld hl, font_4 * 257
    ld (digit_tab + $128), hl
    ld (digit_tab + $12A), hl
    ld (digit_tab + $12C), hl
    ld (digit_tab + $12E), hl
    ld (digit_tab + $130), hl

    ld hl, font_5 * 257
    ld (digit_tab + $132), hl
    ld (digit_tab + $134), hl
    ld (digit_tab + $136), hl
    ld (digit_tab + $138), hl
    ld (digit_tab + $13A), hl

    ld hl, font_6 * 257
    ld (digit_tab + $13C), hl
    ld (digit_tab + $13E), hl
    ld (digit_tab + $140), hl
    ld (digit_tab + $142), hl
    ld (digit_tab + $144), hl

    ld hl, font_7 * 257
    ld (digit_tab + $146), hl
    ld (digit_tab + $148), hl
    ld (digit_tab + $14A), hl
    ld (digit_tab + $14C), hl
    ld (digit_tab + $14E), hl

    ld hl, font_8 * 257
    ld (digit_tab + $150), hl
    ld (digit_tab + $152), hl
    ld (digit_tab + $154), hl
    ld (digit_tab + $156), hl
    ld (digit_tab + $158), hl

    ld hl, font_9 * 257
    ld (digit_tab + $15A), hl
    ld (digit_tab + $15C), hl
    ld (digit_tab + $15E), hl
    ld (digit_tab + $160), hl
    ld (digit_tab + $162), hl

    ld hl, font_0 * 257
    ld (digit_tab + $164), hl
    ld (digit_tab + $166), hl
    ld (digit_tab + $168), hl
    ld (digit_tab + $16A), hl
    ld (digit_tab + $16C), hl

    ld hl, font_1 * 257
    ld (digit_tab + $16E), hl
    ld (digit_tab + $170), hl
    ld (digit_tab + $172), hl
    ld (digit_tab + $174), hl
    ld (digit_tab + $176), hl

    ld hl, font_2 * 257
    ld (digit_tab + $178), hl
    ld (digit_tab + $17A), hl
    ld (digit_tab + $17C), hl
    ld (digit_tab + $17E), hl
    ld (digit_tab + $180), hl

    ld hl, font_3 * 257
    ld (digit_tab + $182), hl
    ld (digit_tab + $184), hl
    ld (digit_tab + $186), hl
    ld (digit_tab + $188), hl
    ld (digit_tab + $18A), hl

    ld hl, font_4 * 257
    ld (digit_tab + $18C), hl
    ld (digit_tab + $18E), hl
    ld (digit_tab + $190), hl
    ld (digit_tab + $192), hl
    ld (digit_tab + $194), hl

    ld hl, font_5 * 257
    ld (digit_tab + $196), hl
    ld (digit_tab + $198), hl
    ld (digit_tab + $19A), hl
    ld (digit_tab + $19C), hl
    ld (digit_tab + $19E), hl

    ld hl, font_6 * 257
    ld (digit_tab + $1A0), hl
    ld (digit_tab + $1A2), hl
    ld (digit_tab + $1A4), hl
    ld (digit_tab + $1A6), hl
    ld (digit_tab + $1A8), hl

    ld hl, font_7 * 257
    ld (digit_tab + $1AA), hl
    ld (digit_tab + $1AC), hl
    ld (digit_tab + $1AE), hl
    ld (digit_tab + $1B0), hl
    ld (digit_tab + $1B2), hl

    ld hl, font_8 * 257
    ld (digit_tab + $1B4), hl
    ld (digit_tab + $1B6), hl
    ld (digit_tab + $1B8), hl
    ld (digit_tab + $1BA), hl
    ld (digit_tab + $1BC), hl

    ld hl, font_9 * 257
    ld (digit_tab + $1BE), hl
    ld (digit_tab + $1C0), hl
    ld (digit_tab + $1C2), hl
    ld (digit_tab + $1C4), hl
    ld (digit_tab + $1C6), hl

    ld hl, font_0 * 257
    ld (digit_tab + $1C8), hl
    ld (digit_tab + $1CA), hl
    ld (digit_tab + $1CC), hl
    ld (digit_tab + $1CE), hl
    ld (digit_tab + $1D0), hl

    ld hl, font_1 * 257
    ld (digit_tab + $1D2), hl
    ld (digit_tab + $1D4), hl
    ld (digit_tab + $1D6), hl
    ld (digit_tab + $1D8), hl
    ld (digit_tab + $1DA), hl

    ld hl, font_2 * 257
    ld (digit_tab + $1DC), hl
    ld (digit_tab + $1DE), hl
    ld (digit_tab + $1E0), hl
    ld (digit_tab + $1E2), hl
    ld (digit_tab + $1E4), hl

    ld hl, font_3 * 257
    ld (digit_tab + $1E6), hl
    ld (digit_tab + $1E8), hl
    ld (digit_tab + $1EA), hl
    ld (digit_tab + $1EC), hl
    ld (digit_tab + $1EE), hl

    ld hl, font_4 * 257
    ld (digit_tab + $1F0), hl
    ld (digit_tab + $1F2), hl
    ld (digit_tab + $1F4), hl
    ld (digit_tab + $1F6), hl
    ld (digit_tab + $1F8), hl

    ld hl, font_5 * 257
    ld (digit_tab + $1FA), hl
    ld (digit_tab + $1FC), hl
    ld (digit_tab + $1FE), hl

    ; digit_tab: least significant digit
    ld hl, font_1 << 8 + font_0: ld (digit_tab + $200), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $202), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $204), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $206), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $208), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $20A), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $20C), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $20E), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $210), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $212), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $214), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $216), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $218), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $21A), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $21C), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $21E), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $220), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $222), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $224), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $226), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $228), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $22A), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $22C), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $22E), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $230), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $232), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $234), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $236), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $238), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $23A), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $23C), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $23E), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $240), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $242), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $244), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $246), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $248), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $24A), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $24C), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $24E), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $250), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $252), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $254), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $256), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $258), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $25A), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $25C), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $25E), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $260), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $262), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $264), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $266), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $268), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $26A), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $26C), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $26E), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $270), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $272), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $274), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $276), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $278), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $27A), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $27C), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $27E), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $280), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $282), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $284), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $286), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $288), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $28A), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $28C), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $28E), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $290), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $292), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $294), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $296), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $298), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $29A), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $29C), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $29E), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2A0), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2A2), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2A4), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $2A6), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $2A8), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2AA), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2AC), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2AE), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $2B0), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $2B2), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2B4), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2B6), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2B8), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $2BA), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $2BC), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2BE), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2C0), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2C2), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $2C4), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $2C6), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2C8), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2CA), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2CC), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $2CE), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $2D0), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2D2), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2D4), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2D6), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $2D8), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $2DA), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2DC), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2DE), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2E0), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $2E2), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $2E4), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2E6), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2E8), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2EA), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $2EC), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $2EE), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2F0), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2F2), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2F4), hl
    ld hl, font_7 << 8 + font_6: ld (digit_tab + $2F6), hl
    ld hl, font_9 << 8 + font_8: ld (digit_tab + $2F8), hl

    ld hl, font_1 << 8 + font_0: ld (digit_tab + $2FA), hl
    ld hl, font_3 << 8 + font_2: ld (digit_tab + $2FC), hl
    ld hl, font_5 << 8 + font_4: ld (digit_tab + $2FE), hl

    ; rand_tab - from random.org
    ld hl, $419F: ld (rand_tab), hl
    ld hl, $290E: ld (rand_tab + $02), hl
    ld hl, $D46B: ld (rand_tab + $04), hl
    ld hl, $5D11: ld (rand_tab + $06), hl
    ld hl, $7300: ld (rand_tab + $08), hl
    ld hl, $25EB: ld (rand_tab + $0A), hl
    ld hl, $83A7: ld (rand_tab + $0C), hl
    ld hl, $D716: ld (rand_tab + $0E), hl
    ld hl, $7F05: ld (rand_tab + $10), hl
    ld hl, $702E: ld (rand_tab + $12), hl
    ld hl, $7C64: ld (rand_tab + $14), hl
    ld hl, $2361: ld (rand_tab + $16), hl
    ld hl, $CB30: ld (rand_tab + $18), hl
    ld hl, $55EB: ld (rand_tab + $1A), hl
    ld hl, $7EF4: ld (rand_tab + $1C), hl
    ld hl, $BC57: ld (rand_tab + $1E), hl

    ld hl, $23B4: ld (rand_tab + $20), hl
    ld hl, $7A20: ld (rand_tab + $22), hl
    ld hl, $749F: ld (rand_tab + $24), hl
    ld hl, $CD8E: ld (rand_tab + $26), hl
    ld hl, $4A86: ld (rand_tab + $28), hl
    ld hl, $CB3E: ld (rand_tab + $2A), hl
    ld hl, $B58E: ld (rand_tab + $2C), hl
    ld hl, $FB77: ld (rand_tab + $2E), hl
    ld hl, $D9D2: ld (rand_tab + $30), hl
    ld hl, $5BEE: ld (rand_tab + $32), hl
    ld hl, $BADE: ld (rand_tab + $34), hl
    ld hl, $02BF: ld (rand_tab + $36), hl
    ld hl, $DB76: ld (rand_tab + $38), hl
    ld hl, $D6A8: ld (rand_tab + $3A), hl
    ld hl, $B2DB: ld (rand_tab + $3C), hl
    ld hl, $ED3B: ld (rand_tab + $3E), hl

    ld hl, $7472: ld (rand_tab + $40), hl
    ld hl, $F91A: ld (rand_tab + $42), hl
    ld hl, $A161: ld (rand_tab + $44), hl
    ld hl, $F7AD: ld (rand_tab + $46), hl
    ld hl, $F4B0: ld (rand_tab + $48), hl
    ld hl, $A6CF: ld (rand_tab + $4A), hl
    ld hl, $7A70: ld (rand_tab + $4C), hl
    ld hl, $DFA4: ld (rand_tab + $4E), hl
    ld hl, $EFBD: ld (rand_tab + $50), hl
    ld hl, $AE2B: ld (rand_tab + $52), hl
    ld hl, $3EC8: ld (rand_tab + $54), hl
    ld hl, $A4B4: ld (rand_tab + $56), hl
    ld hl, $FA3D: ld (rand_tab + $58), hl
    ld hl, $CE0E: ld (rand_tab + $5A), hl
    ld hl, $423F: ld (rand_tab + $5C), hl
    ld hl, $C7BF: ld (rand_tab + $5E), hl

    ld hl, $F594: ld (rand_tab + $60), hl
    ld hl, $5BA4: ld (rand_tab + $62), hl
    ld hl, $70B3: ld (rand_tab + $64), hl
    ld hl, $A09D: ld (rand_tab + $66), hl
    ld hl, $B120: ld (rand_tab + $68), hl
    ld hl, $962B: ld (rand_tab + $6A), hl
    ld hl, $3E62: ld (rand_tab + $6C), hl
    ld hl, $0E2B: ld (rand_tab + $6E), hl
    ld hl, $3BE2: ld (rand_tab + $70), hl
    ld hl, $7C66: ld (rand_tab + $72), hl
    ld hl, $F64B: ld (rand_tab + $74), hl
    ld hl, $D06D: ld (rand_tab + $76), hl
    ld hl, $12A7: ld (rand_tab + $78), hl
    ld hl, $720D: ld (rand_tab + $7A), hl
    ld hl, $F707: ld (rand_tab + $7C), hl
    ld hl, $9C3F: ld (rand_tab + $7E), hl

    ld hl, $2661: ld (rand_tab + $80), hl
    ld hl, $CEA8: ld (rand_tab + $82), hl
    ld hl, $0ADF: ld (rand_tab + $84), hl
    ld hl, $9D88: ld (rand_tab + $86), hl
    ld hl, $B201: ld (rand_tab + $88), hl
    ld hl, $6214: ld (rand_tab + $8A), hl
    ld hl, $A29D: ld (rand_tab + $8C), hl
    ld hl, $2A14: ld (rand_tab + $8E), hl
    ld hl, $F430: ld (rand_tab + $90), hl
    ld hl, $C54B: ld (rand_tab + $92), hl
    ld hl, $3108: ld (rand_tab + $94), hl
    ld hl, $B876: ld (rand_tab + $96), hl
    ld hl, $BDCF: ld (rand_tab + $98), hl
    ld hl, $F3B8: ld (rand_tab + $9A), hl
    ld hl, $E26D: ld (rand_tab + $9C), hl
    ld hl, $FB48: ld (rand_tab + $9E), hl

    ld hl, $0A40: ld (rand_tab + $A0), hl
    ld hl, $3A8A: ld (rand_tab + $A2), hl
    ld hl, $6F1C: ld (rand_tab + $A4), hl
    ld hl, $6EA3: ld (rand_tab + $A6), hl
    ld hl, $6B80: ld (rand_tab + $A8), hl
    ld hl, $68A8: ld (rand_tab + $AA), hl
    ld hl, $19C8: ld (rand_tab + $AC), hl
    ld hl, $FD75: ld (rand_tab + $AE), hl
    ld hl, $7C9D: ld (rand_tab + $B0), hl
    ld hl, $3C7C: ld (rand_tab + $B2), hl
    ld hl, $F68B: ld (rand_tab + $B4), hl
    ld hl, $07C3: ld (rand_tab + $B6), hl
    ld hl, $E557: ld (rand_tab + $B8), hl
    ld hl, $890D: ld (rand_tab + $BA), hl
    ld hl, $D031: ld (rand_tab + $BC), hl
    ld hl, $82C4: ld (rand_tab + $BE), hl

    ld hl, $4E56: ld (rand_tab + $C0), hl
    ld hl, $F02C: ld (rand_tab + $C2), hl
    ld hl, $A0EF: ld (rand_tab + $C4), hl
    ld hl, $9701: ld (rand_tab + $C6), hl
    ld hl, $18E3: ld (rand_tab + $C8), hl
    ld hl, $0030: ld (rand_tab + $CA), hl
    ld hl, $EE97: ld (rand_tab + $CC), hl
    ld hl, $35AC: ld (rand_tab + $CE), hl
    ld hl, $394C: ld (rand_tab + $D0), hl
    ld hl, $988A: ld (rand_tab + $D2), hl
    ld hl, $3C60: ld (rand_tab + $D4), hl
    ld hl, $0352: ld (rand_tab + $D6), hl
    ld hl, $7CBD: ld (rand_tab + $D8), hl
    ld hl, $8251: ld (rand_tab + $DA), hl
    ld hl, $DD2A: ld (rand_tab + $DC), hl
    ld hl, $94A6: ld (rand_tab + $DE), hl

    ld hl, $D8B2: ld (rand_tab + $E0), hl
    ld hl, $979C: ld (rand_tab + $E2), hl
    ld hl, $9EFA: ld (rand_tab + $E4), hl
    ld hl, $CD68: ld (rand_tab + $E6), hl
    ld hl, $8D50: ld (rand_tab + $E8), hl
    ld hl, $3CC5: ld (rand_tab + $EA), hl
    ld hl, $F03C: ld (rand_tab + $EC), hl
    ld hl, $7475: ld (rand_tab + $EE), hl
    ld hl, $70E2: ld (rand_tab + $F0), hl
    ld hl, $7ED7: ld (rand_tab + $F2), hl
    ld hl, $9C90: ld (rand_tab + $F4), hl
    ld hl, $1A62: ld (rand_tab + $F6), hl
    ld hl, $1A8A: ld (rand_tab + $F8), hl
    ld hl, $C3D6: ld (rand_tab + $FA), hl
    ld hl, $8C66: ld (rand_tab + $FC), hl
    ld hl, $02C4: ld (rand_tab + $FE), hl

    ; screen_tab low byte
    ld hl, $8988: ld (screen_tab + $00), hl: ld (screen_tab + $10), hl
    ld hl, $8B8A: ld (screen_tab + $02), hl: ld (screen_tab + $12), hl
    ld hl, $8D8C: ld (screen_tab + $04), hl: ld (screen_tab + $14), hl
    ld hl, $8F8E: ld (screen_tab + $06), hl: ld (screen_tab + $16), hl
    ld hl, $9190: ld (screen_tab + $08), hl: ld (screen_tab + $18), hl
    ld hl, $9392: ld (screen_tab + $0A), hl: ld (screen_tab + $1A), hl
    ld hl, $9594: ld (screen_tab + $0C), hl: ld (screen_tab + $1C), hl
    ld hl, $9796: ld (screen_tab + $0E), hl: ld (screen_tab + $1E), hl

    ld hl, $A9A8: ld (screen_tab + $20), hl: ld (screen_tab + $30), hl
    ld hl, $ABAA: ld (screen_tab + $22), hl: ld (screen_tab + $32), hl
    ld hl, $ADAC: ld (screen_tab + $24), hl: ld (screen_tab + $34), hl
    ld hl, $AFAE: ld (screen_tab + $26), hl: ld (screen_tab + $36), hl
    ld hl, $B1B0: ld (screen_tab + $28), hl: ld (screen_tab + $38), hl
    ld hl, $B3B2: ld (screen_tab + $2A), hl: ld (screen_tab + $3A), hl
    ld hl, $B5B4: ld (screen_tab + $2C), hl: ld (screen_tab + $3C), hl
    ld hl, $B7B6: ld (screen_tab + $2E), hl: ld (screen_tab + $3E), hl

    ld hl, $C9C8: ld (screen_tab + $40), hl: ld (screen_tab + $50), hl
    ld hl, $CBCA: ld (screen_tab + $42), hl: ld (screen_tab + $52), hl
    ld hl, $CDCC: ld (screen_tab + $44), hl: ld (screen_tab + $54), hl
    ld hl, $CFCE: ld (screen_tab + $46), hl: ld (screen_tab + $56), hl
    ld hl, $D1D0: ld (screen_tab + $48), hl: ld (screen_tab + $58), hl
    ld hl, $D3D2: ld (screen_tab + $4A), hl: ld (screen_tab + $5A), hl
    ld hl, $D5D4: ld (screen_tab + $4C), hl: ld (screen_tab + $5C), hl
    ld hl, $D7D6: ld (screen_tab + $4E), hl: ld (screen_tab + $5E), hl

    ld hl, $E9E8: ld (screen_tab + $60), hl: ld (screen_tab + $70), hl
    ld hl, $EBEA: ld (screen_tab + $62), hl: ld (screen_tab + $72), hl
    ld hl, $EDEC: ld (screen_tab + $64), hl: ld (screen_tab + $74), hl
    ld hl, $EFEE: ld (screen_tab + $66), hl: ld (screen_tab + $76), hl
    ld hl, $F1F0: ld (screen_tab + $68), hl: ld (screen_tab + $78), hl
    ld hl, $F3F2: ld (screen_tab + $6A), hl: ld (screen_tab + $7A), hl
    ld hl, $F5F4: ld (screen_tab + $6C), hl: ld (screen_tab + $7C), hl
    ld hl, $F7F6: ld (screen_tab + $6E), hl: ld (screen_tab + $7E), hl

    ld hl, $0908: ld (screen_tab + $80), hl: ld (screen_tab + $90), hl
    ld hl, $0B0A: ld (screen_tab + $82), hl: ld (screen_tab + $92), hl
    ld hl, $0D0C: ld (screen_tab + $84), hl: ld (screen_tab + $94), hl
    ld hl, $0F0E: ld (screen_tab + $86), hl: ld (screen_tab + $96), hl
    ld hl, $1110: ld (screen_tab + $88), hl: ld (screen_tab + $98), hl
    ld hl, $1312: ld (screen_tab + $8A), hl: ld (screen_tab + $9A), hl
    ld hl, $1514: ld (screen_tab + $8C), hl: ld (screen_tab + $9C), hl
    ld hl, $1716: ld (screen_tab + $8E), hl: ld (screen_tab + $9E), hl

    ld hl, $2928: ld (screen_tab + $A0), hl: ld (screen_tab + $B0), hl
    ld hl, $2B2A: ld (screen_tab + $A2), hl: ld (screen_tab + $B2), hl
    ld hl, $2D2C: ld (screen_tab + $A4), hl: ld (screen_tab + $B4), hl
    ld hl, $2F2E: ld (screen_tab + $A6), hl: ld (screen_tab + $B6), hl
    ld hl, $3130: ld (screen_tab + $A8), hl: ld (screen_tab + $B8), hl
    ld hl, $3332: ld (screen_tab + $AA), hl: ld (screen_tab + $BA), hl
    ld hl, $3534: ld (screen_tab + $AC), hl: ld (screen_tab + $BC), hl
    ld hl, $3736: ld (screen_tab + $AE), hl: ld (screen_tab + $BE), hl

    ld hl, $4948: ld (screen_tab + $C0), hl: ld (screen_tab + $D0), hl
    ld hl, $4B4A: ld (screen_tab + $C2), hl: ld (screen_tab + $D2), hl
    ld hl, $4D4C: ld (screen_tab + $C4), hl: ld (screen_tab + $D4), hl
    ld hl, $4F4E: ld (screen_tab + $C6), hl: ld (screen_tab + $D6), hl
    ld hl, $5150: ld (screen_tab + $C8), hl: ld (screen_tab + $D8), hl
    ld hl, $5352: ld (screen_tab + $CA), hl: ld (screen_tab + $DA), hl
    ld hl, $5554: ld (screen_tab + $CC), hl: ld (screen_tab + $DC), hl
    ld hl, $5756: ld (screen_tab + $CE), hl: ld (screen_tab + $DE), hl

    ld hl, $6968: ld (screen_tab + $E0), hl: ld (screen_tab + $F0), hl
    ld hl, $6B6A: ld (screen_tab + $E2), hl: ld (screen_tab + $F2), hl
    ld hl, $6D6C: ld (screen_tab + $E4), hl: ld (screen_tab + $F4), hl
    ld hl, $6F6E: ld (screen_tab + $E6), hl: ld (screen_tab + $F6), hl
    ld hl, $7170: ld (screen_tab + $E8), hl: ld (screen_tab + $F8), hl
    ld hl, $7372: ld (screen_tab + $EA), hl: ld (screen_tab + $FA), hl
    ld hl, $7574: ld (screen_tab + $EC), hl: ld (screen_tab + $FC), hl
    ld hl, $7776: ld (screen_tab + $EE), hl: ld (screen_tab + $FE), hl

    ; screen_tab high byte
    ld hl, $5858
    ld (screen_tab + $100), hl
    ld (screen_tab + $102), hl
    ld (screen_tab + $104), hl
    ld (screen_tab + $106), hl
    ld (screen_tab + $108), hl
    ld (screen_tab + $10A), hl
    ld (screen_tab + $10C), hl
    ld (screen_tab + $10E), hl

    ld (screen_tab + $120), hl
    ld (screen_tab + $122), hl
    ld (screen_tab + $124), hl
    ld (screen_tab + $126), hl
    ld (screen_tab + $128), hl
    ld (screen_tab + $12A), hl
    ld (screen_tab + $12C), hl
    ld (screen_tab + $12E), hl

    ld (screen_tab + $140), hl
    ld (screen_tab + $142), hl
    ld (screen_tab + $144), hl
    ld (screen_tab + $146), hl
    ld (screen_tab + $148), hl
    ld (screen_tab + $14A), hl
    ld (screen_tab + $14C), hl
    ld (screen_tab + $14E), hl

    ld (screen_tab + $160), hl
    ld (screen_tab + $162), hl
    ld (screen_tab + $164), hl
    ld (screen_tab + $166), hl
    ld (screen_tab + $168), hl
    ld (screen_tab + $16A), hl
    ld (screen_tab + $16C), hl
    ld (screen_tab + $16E), hl

    ld hl, $5959
    ld (screen_tab + $110), hl
    ld (screen_tab + $112), hl
    ld (screen_tab + $114), hl
    ld (screen_tab + $116), hl
    ld (screen_tab + $118), hl
    ld (screen_tab + $11A), hl
    ld (screen_tab + $11C), hl
    ld (screen_tab + $11E), hl

    ld (screen_tab + $130), hl
    ld (screen_tab + $132), hl
    ld (screen_tab + $134), hl
    ld (screen_tab + $136), hl
    ld (screen_tab + $138), hl
    ld (screen_tab + $13A), hl
    ld (screen_tab + $13C), hl
    ld (screen_tab + $13E), hl

    ld (screen_tab + $150), hl
    ld (screen_tab + $152), hl
    ld (screen_tab + $154), hl
    ld (screen_tab + $156), hl
    ld (screen_tab + $158), hl
    ld (screen_tab + $15A), hl
    ld (screen_tab + $15C), hl
    ld (screen_tab + $15E), hl

    ld (screen_tab + $170), hl
    ld (screen_tab + $172), hl
    ld (screen_tab + $174), hl
    ld (screen_tab + $176), hl
    ld (screen_tab + $178), hl
    ld (screen_tab + $17A), hl
    ld (screen_tab + $17C), hl
    ld (screen_tab + $17E), hl

    ld (screen_tab + $180), hl
    ld (screen_tab + $182), hl
    ld (screen_tab + $184), hl
    ld (screen_tab + $186), hl
    ld (screen_tab + $188), hl
    ld (screen_tab + $18A), hl
    ld (screen_tab + $18C), hl
    ld (screen_tab + $18E), hl

    ld (screen_tab + $1A0), hl
    ld (screen_tab + $1A2), hl
    ld (screen_tab + $1A4), hl
    ld (screen_tab + $1A6), hl
    ld (screen_tab + $1A8), hl
    ld (screen_tab + $1AA), hl
    ld (screen_tab + $1AC), hl
    ld (screen_tab + $1AE), hl

    ld (screen_tab + $1C0), hl
    ld (screen_tab + $1C2), hl
    ld (screen_tab + $1C4), hl
    ld (screen_tab + $1C6), hl
    ld (screen_tab + $1C8), hl
    ld (screen_tab + $1CA), hl
    ld (screen_tab + $1CC), hl
    ld (screen_tab + $1CE), hl

    ld (screen_tab + $1E0), hl
    ld (screen_tab + $1E2), hl
    ld (screen_tab + $1E4), hl
    ld (screen_tab + $1E6), hl
    ld (screen_tab + $1E8), hl
    ld (screen_tab + $1EA), hl
    ld (screen_tab + $1EC), hl
    ld (screen_tab + $1EE), hl

    ld hl, $5A5A
    ld (screen_tab + $190), hl
    ld (screen_tab + $192), hl
    ld (screen_tab + $194), hl
    ld (screen_tab + $196), hl
    ld (screen_tab + $198), hl
    ld (screen_tab + $19A), hl
    ld (screen_tab + $19C), hl
    ld (screen_tab + $19E), hl

    ld (screen_tab + $1B0), hl
    ld (screen_tab + $1B2), hl
    ld (screen_tab + $1B4), hl
    ld (screen_tab + $1B6), hl
    ld (screen_tab + $1B8), hl
    ld (screen_tab + $1BA), hl
    ld (screen_tab + $1BC), hl
    ld (screen_tab + $1BE), hl

    ld (screen_tab + $1D0), hl
    ld (screen_tab + $1D2), hl
    ld (screen_tab + $1D4), hl
    ld (screen_tab + $1D6), hl
    ld (screen_tab + $1D8), hl
    ld (screen_tab + $1DA), hl
    ld (screen_tab + $1DC), hl
    ld (screen_tab + $1DE), hl

    ld (screen_tab + $1F0), hl
    ld (screen_tab + $1F2), hl
    ld (screen_tab + $1F4), hl
    ld (screen_tab + $1F6), hl
    ld (screen_tab + $1F8), hl
    ld (screen_tab + $1FA), hl
    ld (screen_tab + $1FC), hl
    ld (screen_tab + $1FE), hl

    ; TO MAYBE DO - no need for separate high score variables
    ; but I really don't want to rewrite any of this
    ld a, 0
    ld (new_hi), a

    ld a, 7 ; SPEED OF GAME - lower means faster
    ld (delay_length), a
    ld (delay_counter), a

    ; border around edge
    ld a, border_colour
    ; top border
    ld ($5867), a
    ld ($5868), a
    ld ($5869), a
    ld ($586A), a
    ld ($586B), a
    ld ($586C), a
    ld ($586D), a
    ld ($586E), a
    ld ($586F), a
    ld ($5870), a
    ld ($5871), a
    ld ($5872), a
    ld ($5873), a
    ld ($5874), a
    ld ($5875), a
    ld ($5876), a
    ld ($5877), a
    ld ($5878), a

    ; left border
    ld ($5887), a
    ld ($58A7), a
    ld ($58C7), a
    ld ($58E7), a
    ld ($5907), a
    ld ($5927), a
    ld ($5947), a
    ld ($5967), a
    ld ($5987), a
    ld ($59A7), a
    ld ($59C7), a
    ld ($59E7), a
    ld ($5A07), a
    ld ($5A27), a
    ld ($5A47), a
    ld ($5A67), a

    ; right border
    ld ($5898), a
    ld ($58B8), a
    ld ($58D8), a
    ld ($58F8), a
    ld ($5918), a
    ld ($5938), a
    ld ($5958), a
    ld ($5978), a
    ld ($5998), a
    ld ($59B8), a
    ld ($59D8), a
    ld ($59F8), a
    ld ($5A18), a
    ld ($5A38), a
    ld ($5A58), a
    ld ($5A78), a

    ; bottom border
    ld ($5A87), a
    ld ($5A88), a
    ld ($5A89), a
    ld ($5A8A), a
    ld ($5A8B), a
    ld ($5A8C), a
    ld ($5A8D), a
    ld ($5A8E), a
    ld ($5A8F), a
    ld ($5A90), a
    ld ($5A91), a
    ld ($5A92), a
    ld ($5A93), a
    ld ($5A94), a
    ld ($5A95), a
    ld ($5A96), a
    ld ($5A97), a
    ld ($5A98), a

    ld a, 0
    ld (next_lev_flag), a
    ld a, starting_level
    ld (current_level), a

    ; setup level tables
    ld hl, 0                     ; space
    ld bc, obstacle_colour       ; on left
    ld de, obstacle_colour * 256 ; on right
    ld ix, obstacle_colour * 257 ; double

    ld (level0), hl
    ld (level0 + $02), hl
    ld (level0 + $04), hl
    ld (level0 + $06), hl
    ld (level0 + $08), hl
    ld (level0 + $0A), hl
    ld (level0 + $0C), hl
    ld (level0 + $0E), hl

    ld (level0 + $10), hl
    ld (level0 + $12), hl
    ld (level0 + $14), hl
    ld (level0 + $16), hl
    ld (level0 + $18), hl
    ld (level0 + $1A), hl
    ld (level0 + $1C), hl
    ld (level0 + $1E), hl

    ld (level0 + $20), hl
    ld (level0 + $22), hl
    ld (level0 + $24), hl
    ld (level0 + $26), hl
    ld (level0 + $28), hl
    ld (level0 + $2A), hl
    ld (level0 + $2C), hl
    ld (level0 + $2E), hl

    ld (level0 + $30), hl
    ld (level0 + $32), hl
    ld (level0 + $34), hl
    ld (level0 + $36), hl
    ld (level0 + $38), hl
    ld (level0 + $3A), hl
    ld (level0 + $3C), hl
    ld (level0 + $3E), hl

    ld (level0 + $40), hl
    ld (level0 + $42), hl
    ld (level0 + $44), hl
    ld (level0 + $46), hl
    ld (level0 + $48), hl
    ld (level0 + $4A), hl
    ld (level0 + $4C), hl
    ld (level0 + $4E), hl

    ld (level0 + $50), hl
    ld (level0 + $52), hl
    ld (level0 + $54), hl
    ld (level0 + $56), hl
    ld (level0 + $58), hl
    ld (level0 + $5A), hl
    ld (level0 + $5C), hl
    ld (level0 + $5E), hl

    ld (level0 + $60), hl
    ld (level0 + $62), hl
    ld (level0 + $64), hl
    ld (level0 + $66), hl
    ld (level0 + $68), hl
    ld (level0 + $6A), hl
    ld (level0 + $6C), hl
    ld (level0 + $6E), hl

    ld (level0 + $70), hl
    ld (level0 + $72), hl
    ld (level0 + $74), hl
    ld (level0 + $76), hl
    ld (level0 + $78), hl
    ld (level0 + $7A), hl
    ld (level0 + $7C), hl
    ld (level0 + $7E), hl

    ld (level0 + $80), hl
    ld (level0 + $82), hl
    ld (level0 + $84), hl
    ld (level0 + $86), hl
    ld (level0 + $88), hl
    ld (level0 + $8A), hl
    ld (level0 + $8C), hl
    ld (level0 + $8E), hl

    ld (level0 + $90), hl
    ld (level0 + $92), hl
    ld (level0 + $94), hl
    ld (level0 + $96), hl
    ld (level0 + $98), hl
    ld (level0 + $9A), hl
    ld (level0 + $9C), hl
    ld (level0 + $9E), hl

    ld (level0 + $A0), hl
    ld (level0 + $A2), hl
    ld (level0 + $A4), hl
    ld (level0 + $A6), hl
    ld (level0 + $A8), hl
    ld (level0 + $AA), hl
    ld (level0 + $AC), hl
    ld (level0 + $AE), hl

    ld (level0 + $B0), hl
    ld (level0 + $B2), hl
    ld (level0 + $B4), hl
    ld (level0 + $B6), hl
    ld (level0 + $B8), hl
    ld (level0 + $BA), hl
    ld (level0 + $BC), hl
    ld (level0 + $BE), hl

    ld (level0 + $C0), hl
    ld (level0 + $C2), hl
    ld (level0 + $C4), hl
    ld (level0 + $C6), hl
    ld (level0 + $C8), hl
    ld (level0 + $CA), hl
    ld (level0 + $CC), hl
    ld (level0 + $CE), hl

    ld (level0 + $D0), hl
    ld (level0 + $D2), hl
    ld (level0 + $D4), hl
    ld (level0 + $D6), hl
    ld (level0 + $D8), hl
    ld (level0 + $DA), hl
    ld (level0 + $DC), hl
    ld (level0 + $DE), hl

    ld (level0 + $E0), hl
    ld (level0 + $E2), hl
    ld (level0 + $E4), hl
    ld (level0 + $E6), hl
    ld (level0 + $E8), hl
    ld (level0 + $EA), hl
    ld (level0 + $EC), hl
    ld (level0 + $EE), hl

    ld (level0 + $F0), hl
    ld (level0 + $F2), hl
    ld (level0 + $F4), hl
    ld (level0 + $F6), hl
    ld (level0 + $F8), hl
    ld (level0 + $FA), hl
    ld (level0 + $FC), hl
    ld (level0 + $FE), hl

    ; level1
    ld (level1), hl
    ld (level1 + $02), hl
    ld (level1 + $04), hl
    ld (level1 + $06), hl
    ld (level1 + $08), hl
    ld (level1 + $0A), hl
    ld (level1 + $0C), hl
    ld (level1 + $0E), hl

    ld (level1 + $10), hl
    ld (level1 + $12), hl
    ld (level1 + $14), hl
    ld (level1 + $16), hl
    ld (level1 + $18), hl
    ld (level1 + $1A), hl
    ld (level1 + $1C), hl
    ld (level1 + $1E), hl

    ld (level1 + $20), hl
    ld (level1 + $22), hl
    ld (level1 + $24), hl
    ld (level1 + $26), hl
    ld (level1 + $28), hl
    ld (level1 + $2A), hl
    ld (level1 + $2C), hl
    ld (level1 + $2E), hl

    ld (level1 + $30), hl
    ld (level1 + $32), hl
    ld (level1 + $34), hl
    ld (level1 + $36), hl
    ld (level1 + $38), hl
    ld (level1 + $3A), hl
    ld (level1 + $3C), hl
    ld (level1 + $3E), hl

    ld (level1 + $40), hl
    ld (level1 + $42), hl
    ld (level1 + $44), de
    ld (level1 + $46), hl
    ld (level1 + $48), hl
    ld (level1 + $4A), bc
    ld (level1 + $4C), hl
    ld (level1 + $4E), hl

    ld (level1 + $50), hl
    ld (level1 + $52), hl
    ld (level1 + $54), de
    ld (level1 + $56), hl
    ld (level1 + $58), hl
    ld (level1 + $5A), bc
    ld (level1 + $5C), hl
    ld (level1 + $5E), hl

    ld (level1 + $60), hl
    ld (level1 + $62), hl
    ld (level1 + $64), hl
    ld (level1 + $66), hl
    ld (level1 + $68), hl
    ld (level1 + $6A), hl
    ld (level1 + $6C), hl
    ld (level1 + $6E), hl

    ld (level1 + $70), hl
    ld (level1 + $72), hl
    ld (level1 + $74), hl
    ld (level1 + $76), hl
    ld (level1 + $78), hl
    ld (level1 + $7A), hl
    ld (level1 + $7C), hl
    ld (level1 + $7E), hl

    ld (level1 + $80), hl
    ld (level1 + $82), hl
    ld (level1 + $84), hl
    ld (level1 + $86), hl
    ld (level1 + $88), hl
    ld (level1 + $8A), hl
    ld (level1 + $8C), hl
    ld (level1 + $8E), hl

    ld (level1 + $90), hl
    ld (level1 + $92), hl
    ld (level1 + $94), hl
    ld (level1 + $96), hl
    ld (level1 + $98), hl
    ld (level1 + $9A), hl
    ld (level1 + $9C), hl
    ld (level1 + $9E), hl

    ld (level1 + $A0), hl
    ld (level1 + $A2), hl
    ld (level1 + $A4), de
    ld (level1 + $A6), hl
    ld (level1 + $A8), hl
    ld (level1 + $AA), bc
    ld (level1 + $AC), hl
    ld (level1 + $AE), hl

    ld (level1 + $B0), hl
    ld (level1 + $B2), hl
    ld (level1 + $B4), de
    ld (level1 + $B6), hl
    ld (level1 + $B8), hl
    ld (level1 + $BA), bc
    ld (level1 + $BC), hl
    ld (level1 + $BE), hl

    ld (level1 + $C0), hl
    ld (level1 + $C2), hl
    ld (level1 + $C4), hl
    ld (level1 + $C6), hl
    ld (level1 + $C8), hl
    ld (level1 + $CA), hl
    ld (level1 + $CC), hl
    ld (level1 + $CE), hl

    ld (level1 + $D0), hl
    ld (level1 + $D2), hl
    ld (level1 + $D4), hl
    ld (level1 + $D6), hl
    ld (level1 + $D8), hl
    ld (level1 + $DA), hl
    ld (level1 + $DC), hl
    ld (level1 + $DE), hl

    ld (level1 + $E0), hl
    ld (level1 + $E2), hl
    ld (level1 + $E4), hl
    ld (level1 + $E6), hl
    ld (level1 + $E8), hl
    ld (level1 + $EA), hl
    ld (level1 + $EC), hl
    ld (level1 + $EE), hl

    ld (level1 + $F0), hl
    ld (level1 + $F2), hl
    ld (level1 + $F4), hl
    ld (level1 + $F6), hl
    ld (level1 + $F8), hl
    ld (level1 + $FA), hl
    ld (level1 + $FC), hl
    ld (level1 + $FE), hl

    ; level2
    ld (level2), hl
    ld (level2 + $02), hl
    ld (level2 + $04), hl
    ld (level2 + $06), hl
    ld (level2 + $08), hl
    ld (level2 + $0A), hl
    ld (level2 + $0C), hl
    ld (level2 + $0E), hl

    ld (level2 + $10), hl
    ld (level2 + $12), hl
    ld (level2 + $14), hl
    ld (level2 + $16), hl
    ld (level2 + $18), hl
    ld (level2 + $1A), hl
    ld (level2 + $1C), hl
    ld (level2 + $1E), hl

    ld (level2 + $20), hl
    ld (level2 + $22), hl
    ld (level2 + $24), hl
    ld (level2 + $26), hl
    ld (level2 + $28), hl
    ld (level2 + $2A), hl
    ld (level2 + $2C), hl
    ld (level2 + $2E), hl

    ld (level2 + $30), hl
    ld (level2 + $32), hl
    ld (level2 + $34), hl
    ld (level2 + $36), hl
    ld (level2 + $38), hl
    ld (level2 + $3A), hl
    ld (level2 + $3C), hl
    ld (level2 + $3E), hl

    ld (level2 + $40), hl
    ld (level2 + $42), hl
    ld (level2 + $44), de
    ld (level2 + $46), hl
    ld (level2 + $48), hl
    ld (level2 + $4A), bc
    ld (level2 + $4C), hl
    ld (level2 + $4E), hl

    ld (level2 + $50), hl
    ld (level2 + $52), hl
    ld (level2 + $54), de
    ld (level2 + $56), hl
    ld (level2 + $58), hl
    ld (level2 + $5A), bc
    ld (level2 + $5C), hl
    ld (level2 + $5E), hl

    ld (level2 + $60), hl
    ld (level2 + $62), bc
    ld (level2 + $64), hl
    ld (level2 + $66), hl
    ld (level2 + $68), hl
    ld (level2 + $6A), hl
    ld (level2 + $6C), de
    ld (level2 + $6E), hl

    ld (level2 + $70), hl
    ld (level2 + $72), bc
    ld (level2 + $74), hl
    ld (level2 + $76), hl
    ld (level2 + $78), hl
    ld (level2 + $7A), hl
    ld (level2 + $7C), de
    ld (level2 + $7E), hl

    ld (level2 + $80), hl
    ld (level2 + $82), bc
    ld (level2 + $84), hl
    ld (level2 + $86), hl
    ld (level2 + $88), hl
    ld (level2 + $8A), hl
    ld (level2 + $8C), de
    ld (level2 + $8E), hl

    ld (level2 + $90), hl
    ld (level2 + $92), bc
    ld (level2 + $94), hl
    ld (level2 + $96), hl
    ld (level2 + $98), hl
    ld (level2 + $9A), hl
    ld (level2 + $9C), de
    ld (level2 + $9E), hl

    ld (level2 + $A0), hl
    ld (level2 + $A2), hl
    ld (level2 + $A4), de
    ld (level2 + $A6), hl
    ld (level2 + $A8), hl
    ld (level2 + $AA), bc
    ld (level2 + $AC), hl
    ld (level2 + $AE), hl

    ld (level2 + $B0), hl
    ld (level2 + $B2), hl
    ld (level2 + $B4), de
    ld (level2 + $B6), hl
    ld (level2 + $B8), hl
    ld (level2 + $BA), bc
    ld (level2 + $BC), hl
    ld (level2 + $BE), hl

    ld (level2 + $C0), hl
    ld (level2 + $C2), hl
    ld (level2 + $C4), hl
    ld (level2 + $C6), hl
    ld (level2 + $C8), hl
    ld (level2 + $CA), hl
    ld (level2 + $CC), hl
    ld (level2 + $CE), hl

    ld (level2 + $D0), hl
    ld (level2 + $D2), hl
    ld (level2 + $D4), hl
    ld (level2 + $D6), hl
    ld (level2 + $D8), hl
    ld (level2 + $DA), hl
    ld (level2 + $DC), hl
    ld (level2 + $DE), hl

    ld (level2 + $E0), hl
    ld (level2 + $E2), hl
    ld (level2 + $E4), hl
    ld (level2 + $E6), hl
    ld (level2 + $E8), hl
    ld (level2 + $EA), hl
    ld (level2 + $EC), hl
    ld (level2 + $EE), hl

    ld (level2 + $F0), hl
    ld (level2 + $F2), hl
    ld (level2 + $F4), hl
    ld (level2 + $F6), hl
    ld (level2 + $F8), hl
    ld (level2 + $FA), hl
    ld (level2 + $FC), hl
    ld (level2 + $FE), hl


rept main - * ; DUMMY INSTRUCTIONS
    ld a, a
mend

org main
    ld sp, goto_stack

    ld a, (FRAMES) ; get random number
    ; ld a, r
    ld (rand_ptr), a

    ; draw level
    ld a, (current_level)
    ld h, a

    ld l, $00: ld a, (hl): ld ($5888), a
    ld l, $01: ld a, (hl): ld ($5889), a
    ld l, $02: ld a, (hl): ld ($588A), a
    ld l, $03: ld a, (hl): ld ($588B), a
    ld l, $04: ld a, (hl): ld ($588C), a
    ld l, $05: ld a, (hl): ld ($588D), a
    ld l, $06: ld a, (hl): ld ($588E), a
    ld l, $07: ld a, (hl): ld ($588F), a
    ld l, $08: ld a, (hl): ld ($5890), a
    ld l, $09: ld a, (hl): ld ($5891), a
    ld l, $0A: ld a, (hl): ld ($5892), a
    ld l, $0B: ld a, (hl): ld ($5893), a
    ld l, $0C: ld a, (hl): ld ($5894), a
    ld l, $0D: ld a, (hl): ld ($5895), a
    ld l, $0E: ld a, (hl): ld ($5896), a
    ld l, $0F: ld a, (hl): ld ($5897), a

    ld l, $10: ld a, (hl): ld ($58A8), a
    ld l, $11: ld a, (hl): ld ($58A9), a
    ld l, $12: ld a, (hl): ld ($58AA), a
    ld l, $13: ld a, (hl): ld ($58AB), a
    ld l, $14: ld a, (hl): ld ($58AC), a
    ld l, $15: ld a, (hl): ld ($58AD), a
    ld l, $16: ld a, (hl): ld ($58AE), a
    ld l, $17: ld a, (hl): ld ($58AF), a
    ld l, $18: ld a, (hl): ld ($58B0), a
    ld l, $19: ld a, (hl): ld ($58B1), a
    ld l, $1A: ld a, (hl): ld ($58B2), a
    ld l, $1B: ld a, (hl): ld ($58B3), a
    ld l, $1C: ld a, (hl): ld ($58B4), a
    ld l, $1D: ld a, (hl): ld ($58B5), a
    ld l, $1E: ld a, (hl): ld ($58B6), a
    ld l, $1F: ld a, (hl): ld ($58B7), a

    ld l, $20: ld a, (hl): ld ($58C8), a
    ld l, $21: ld a, (hl): ld ($58C9), a
    ld l, $22: ld a, (hl): ld ($58CA), a
    ld l, $23: ld a, (hl): ld ($58CB), a
    ld l, $24: ld a, (hl): ld ($58CC), a
    ld l, $25: ld a, (hl): ld ($58CD), a
    ld l, $26: ld a, (hl): ld ($58CE), a
    ld l, $27: ld a, (hl): ld ($58CF), a
    ld l, $28: ld a, (hl): ld ($58D0), a
    ld l, $29: ld a, (hl): ld ($58D1), a
    ld l, $2A: ld a, (hl): ld ($58D2), a
    ld l, $2B: ld a, (hl): ld ($58D3), a
    ld l, $2C: ld a, (hl): ld ($58D4), a
    ld l, $2D: ld a, (hl): ld ($58D5), a
    ld l, $2E: ld a, (hl): ld ($58D6), a
    ld l, $2F: ld a, (hl): ld ($58D7), a

    ld l, $30: ld a, (hl): ld ($58E8), a
    ld l, $31: ld a, (hl): ld ($58E9), a
    ld l, $32: ld a, (hl): ld ($58EA), a
    ld l, $33: ld a, (hl): ld ($58EB), a
    ld l, $34: ld a, (hl): ld ($58EC), a
    ld l, $35: ld a, (hl): ld ($58ED), a
    ld l, $36: ld a, (hl): ld ($58EE), a
    ld l, $37: ld a, (hl): ld ($58EF), a
    ld l, $38: ld a, (hl): ld ($58F0), a
    ld l, $39: ld a, (hl): ld ($58F1), a
    ld l, $3A: ld a, (hl): ld ($58F2), a
    ld l, $3B: ld a, (hl): ld ($58F3), a
    ld l, $3C: ld a, (hl): ld ($58F4), a
    ld l, $3D: ld a, (hl): ld ($58F5), a
    ld l, $3E: ld a, (hl): ld ($58F6), a
    ld l, $3F: ld a, (hl): ld ($58F7), a

    ld l, $40: ld a, (hl): ld ($5908), a
    ld l, $41: ld a, (hl): ld ($5909), a
    ld l, $42: ld a, (hl): ld ($590A), a
    ld l, $43: ld a, (hl): ld ($590B), a
    ld l, $44: ld a, (hl): ld ($590C), a
    ld l, $45: ld a, (hl): ld ($590D), a
    ld l, $46: ld a, (hl): ld ($590E), a
    ld l, $47: ld a, (hl): ld ($590F), a
    ld l, $48: ld a, (hl): ld ($5910), a
    ld l, $49: ld a, (hl): ld ($5911), a
    ld l, $4A: ld a, (hl): ld ($5912), a
    ld l, $4B: ld a, (hl): ld ($5913), a
    ld l, $4C: ld a, (hl): ld ($5914), a
    ld l, $4D: ld a, (hl): ld ($5915), a
    ld l, $4E: ld a, (hl): ld ($5916), a
    ld l, $4F: ld a, (hl): ld ($5917), a

    ld l, $50: ld a, (hl): ld ($5928), a
    ld l, $51: ld a, (hl): ld ($5929), a
    ld l, $52: ld a, (hl): ld ($592A), a
    ld l, $53: ld a, (hl): ld ($592B), a
    ld l, $54: ld a, (hl): ld ($592C), a
    ld l, $55: ld a, (hl): ld ($592D), a
    ld l, $56: ld a, (hl): ld ($592E), a
    ld l, $57: ld a, (hl): ld ($592F), a
    ld l, $58: ld a, (hl): ld ($5930), a
    ld l, $59: ld a, (hl): ld ($5931), a
    ld l, $5A: ld a, (hl): ld ($5932), a
    ld l, $5B: ld a, (hl): ld ($5933), a
    ld l, $5C: ld a, (hl): ld ($5934), a
    ld l, $5D: ld a, (hl): ld ($5935), a
    ld l, $5E: ld a, (hl): ld ($5936), a
    ld l, $5F: ld a, (hl): ld ($5937), a

    ld l, $60: ld a, (hl): ld ($5948), a
    ld l, $61: ld a, (hl): ld ($5949), a
    ld l, $62: ld a, (hl): ld ($594A), a
    ld l, $63: ld a, (hl): ld ($594B), a
    ld l, $64: ld a, (hl): ld ($594C), a
    ld l, $65: ld a, (hl): ld ($594D), a
    ld l, $66: ld a, (hl): ld ($594E), a
    ld l, $67: ld a, (hl): ld ($594F), a
    ld l, $68: ld a, (hl): ld ($5950), a
    ld l, $69: ld a, (hl): ld ($5951), a
    ld l, $6A: ld a, (hl): ld ($5952), a
    ld l, $6B: ld a, (hl): ld ($5953), a
    ld l, $6C: ld a, (hl): ld ($5954), a
    ld l, $6D: ld a, (hl): ld ($5955), a
    ld l, $6E: ld a, (hl): ld ($5956), a
    ld l, $6F: ld a, (hl): ld ($5957), a

    ld l, $70: ld a, (hl): ld ($5968), a
    ld l, $71: ld a, (hl): ld ($5969), a
    ld l, $72: ld a, (hl): ld ($596A), a
    ld l, $73: ld a, (hl): ld ($596B), a
    ld l, $74: ld a, (hl): ld ($596C), a
    ld l, $75: ld a, (hl): ld ($596D), a
    ld l, $76: ld a, (hl): ld ($596E), a
    ld l, $77: ld a, (hl): ld ($596F), a
    ld l, $78: ld a, (hl): ld ($5970), a
    ld l, $79: ld a, (hl): ld ($5971), a
    ld l, $7A: ld a, (hl): ld ($5972), a
    ld l, $7B: ld a, (hl): ld ($5973), a
    ld l, $7C: ld a, (hl): ld ($5974), a
    ld l, $7D: ld a, (hl): ld ($5975), a
    ld l, $7E: ld a, (hl): ld ($5976), a
    ld l, $7F: ld a, (hl): ld ($5977), a

    ld l, $80: ld a, (hl): ld ($5988), a
    ld l, $81: ld a, (hl): ld ($5989), a
    ld l, $82: ld a, (hl): ld ($598A), a
    ld l, $83: ld a, (hl): ld ($598B), a
    ld l, $84: ld a, (hl): ld ($598C), a
    ld l, $85: ld a, (hl): ld ($598D), a
    ld l, $86: ld a, (hl): ld ($598E), a
    ld l, $87: ld a, (hl): ld ($598F), a
    ld l, $88: ld a, (hl): ld ($5990), a
    ld l, $89: ld a, (hl): ld ($5991), a
    ld l, $8A: ld a, (hl): ld ($5992), a
    ld l, $8B: ld a, (hl): ld ($5993), a
    ld l, $8C: ld a, (hl): ld ($5994), a
    ld l, $8D: ld a, (hl): ld ($5995), a
    ld l, $8E: ld a, (hl): ld ($5996), a
    ld l, $8F: ld a, (hl): ld ($5997), a

    ld l, $90: ld a, (hl): ld ($59A8), a
    ld l, $91: ld a, (hl): ld ($59A9), a
    ld l, $92: ld a, (hl): ld ($59AA), a
    ld l, $93: ld a, (hl): ld ($59AB), a
    ld l, $94: ld a, (hl): ld ($59AC), a
    ld l, $95: ld a, (hl): ld ($59AD), a
    ld l, $96: ld a, (hl): ld ($59AE), a
    ld l, $97: ld a, (hl): ld ($59AF), a
    ld l, $98: ld a, (hl): ld ($59B0), a
    ld l, $99: ld a, (hl): ld ($59B1), a
    ld l, $9A: ld a, (hl): ld ($59B2), a
    ld l, $9B: ld a, (hl): ld ($59B3), a
    ld l, $9C: ld a, (hl): ld ($59B4), a
    ld l, $9D: ld a, (hl): ld ($59B5), a
    ld l, $9E: ld a, (hl): ld ($59B6), a
    ld l, $9F: ld a, (hl): ld ($59B7), a

    ld l, $A0: ld a, (hl): ld ($59C8), a
    ld l, $A1: ld a, (hl): ld ($59C9), a
    ld l, $A2: ld a, (hl): ld ($59CA), a
    ld l, $A3: ld a, (hl): ld ($59CB), a
    ld l, $A4: ld a, (hl): ld ($59CC), a
    ld l, $A5: ld a, (hl): ld ($59CD), a
    ld l, $A6: ld a, (hl): ld ($59CE), a
    ld l, $A7: ld a, (hl): ld ($59CF), a
    ld l, $A8: ld a, (hl): ld ($59D0), a
    ld l, $A9: ld a, (hl): ld ($59D1), a
    ld l, $AA: ld a, (hl): ld ($59D2), a
    ld l, $AB: ld a, (hl): ld ($59D3), a
    ld l, $AC: ld a, (hl): ld ($59D4), a
    ld l, $AD: ld a, (hl): ld ($59D5), a
    ld l, $AE: ld a, (hl): ld ($59D6), a
    ld l, $AF: ld a, (hl): ld ($59D7), a

    ld l, $B0: ld a, (hl): ld ($59E8), a
    ld l, $B1: ld a, (hl): ld ($59E9), a
    ld l, $B2: ld a, (hl): ld ($59EA), a
    ld l, $B3: ld a, (hl): ld ($59EB), a
    ld l, $B4: ld a, (hl): ld ($59EC), a
    ld l, $B5: ld a, (hl): ld ($59ED), a
    ld l, $B6: ld a, (hl): ld ($59EE), a
    ld l, $B7: ld a, (hl): ld ($59EF), a
    ld l, $B8: ld a, (hl): ld ($59F0), a
    ld l, $B9: ld a, (hl): ld ($59F1), a
    ld l, $BA: ld a, (hl): ld ($59F2), a
    ld l, $BB: ld a, (hl): ld ($59F3), a
    ld l, $BC: ld a, (hl): ld ($59F4), a
    ld l, $BD: ld a, (hl): ld ($59F5), a
    ld l, $BE: ld a, (hl): ld ($59F6), a
    ld l, $BF: ld a, (hl): ld ($59F7), a

    ld l, $C0: ld a, (hl): ld ($5A08), a
    ld l, $C1: ld a, (hl): ld ($5A09), a
    ld l, $C2: ld a, (hl): ld ($5A0A), a
    ld l, $C3: ld a, (hl): ld ($5A0B), a
    ld l, $C4: ld a, (hl): ld ($5A0C), a
    ld l, $C5: ld a, (hl): ld ($5A0D), a
    ld l, $C6: ld a, (hl): ld ($5A0E), a
    ld l, $C7: ld a, (hl): ld ($5A0F), a
    ld l, $C8: ld a, (hl): ld ($5A10), a
    ld l, $C9: ld a, (hl): ld ($5A11), a
    ld l, $CA: ld a, (hl): ld ($5A12), a
    ld l, $CB: ld a, (hl): ld ($5A13), a
    ld l, $CC: ld a, (hl): ld ($5A14), a
    ld l, $CD: ld a, (hl): ld ($5A15), a
    ld l, $CE: ld a, (hl): ld ($5A16), a
    ld l, $CF: ld a, (hl): ld ($5A17), a

    ld l, $D0: ld a, (hl): ld ($5A28), a
    ld l, $D1: ld a, (hl): ld ($5A29), a
    ld l, $D2: ld a, (hl): ld ($5A2A), a
    ld l, $D3: ld a, (hl): ld ($5A2B), a
    ld l, $D4: ld a, (hl): ld ($5A2C), a
    ld l, $D5: ld a, (hl): ld ($5A2D), a
    ld l, $D6: ld a, (hl): ld ($5A2E), a
    ld l, $D7: ld a, (hl): ld ($5A2F), a
    ld l, $D8: ld a, (hl): ld ($5A30), a
    ld l, $D9: ld a, (hl): ld ($5A31), a
    ld l, $DA: ld a, (hl): ld ($5A32), a
    ld l, $DB: ld a, (hl): ld ($5A33), a
    ld l, $DC: ld a, (hl): ld ($5A34), a
    ld l, $DD: ld a, (hl): ld ($5A35), a
    ld l, $DE: ld a, (hl): ld ($5A36), a
    ld l, $DF: ld a, (hl): ld ($5A37), a

    ld l, $E0: ld a, (hl): ld ($5A48), a
    ld l, $E1: ld a, (hl): ld ($5A49), a
    ld l, $E2: ld a, (hl): ld ($5A4A), a
    ld l, $E3: ld a, (hl): ld ($5A4B), a
    ld l, $E4: ld a, (hl): ld ($5A4C), a
    ld l, $E5: ld a, (hl): ld ($5A4D), a
    ld l, $E6: ld a, (hl): ld ($5A4E), a
    ld l, $E7: ld a, (hl): ld ($5A4F), a
    ld l, $E8: ld a, (hl): ld ($5A50), a
    ld l, $E9: ld a, (hl): ld ($5A51), a
    ld l, $EA: ld a, (hl): ld ($5A52), a
    ld l, $EB: ld a, (hl): ld ($5A53), a
    ld l, $EC: ld a, (hl): ld ($5A54), a
    ld l, $ED: ld a, (hl): ld ($5A55), a
    ld l, $EE: ld a, (hl): ld ($5A56), a
    ld l, $EF: ld a, (hl): ld ($5A57), a

    ld l, $F0: ld a, (hl): ld ($5A68), a
    ld l, $F1: ld a, (hl): ld ($5A69), a
    ld l, $F2: ld a, (hl): ld ($5A6A), a
    ld l, $F3: ld a, (hl): ld ($5A6B), a
    ld l, $F4: ld a, (hl): ld ($5A6C), a
    ld l, $F5: ld a, (hl): ld ($5A6D), a
    ld l, $F6: ld a, (hl): ld ($5A6E), a
    ld l, $F7: ld a, (hl): ld ($5A6F), a
    ld l, $F8: ld a, (hl): ld ($5A70), a
    ld l, $F9: ld a, (hl): ld ($5A71), a
    ld l, $FA: ld a, (hl): ld ($5A72), a
    ld l, $FB: ld a, (hl): ld ($5A73), a
    ld l, $FC: ld a, (hl): ld ($5A74), a
    ld l, $FD: ld a, (hl): ld ($5A75), a
    ld l, $FE: ld a, (hl): ld ($5A76), a
    ld l, $FF: ld a, (hl): ld ($5A77), a

    ld a, (new_hi)
    ld (old_hi), a

    ; print hi score - pretty much same as for score
    ld a, (old_hi) ; does it matter whether we print old_hi or new_hi ???
    ld b, font
    ld h, HIGH inc_tab
    ld e, a
    ld d, HIGH digit_tab
    ld a, (de) ; get first digit
    ld c, a

    ; print first char
    ld a, (bc)
    ld (hiscore_screen), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + $100), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + $200), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + $300), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + $400), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + $500), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + $600), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + $700), a

    ld d, (HIGH digit_tab) + 1
    ld a, (de) ; get second digit
    ld c, a

    ; print second char
    ld a, (bc)
    ld (hiscore_screen + 1), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 1 + $100), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 1 + $200), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 1 + $300), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 1 + $400), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 1 + $500), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 1 + $600), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 1 + $700), a

    ld d, (HIGH digit_tab) + 2
    ld a, (de) ; get third digit
    ld c, a

    ; print third char
    ld a, (bc)
    ld (hiscore_screen + 2), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 2 + $100), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 2 + $200), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 2 + $300), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 2 + $400), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 2 + $500), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 2 + $600), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (hiscore_screen + 2 + $700), a

    ; if next_lev_flag, score = score else score = 0
    ld h, HIGH scratch
    ld l, 0
    ld (hl), l
    ld l, 1
    ld a, (score)
    ld (hl), a
    ld a, (next_lev_flag)
    ld l, a
    ld a, (hl)
    ld (score), a

    ; if next_lev_flag, ++current_level else current_level = starting_level
    ld l, 0
    ld (hl), starting_level
    ld d, HIGH inc_tab
    ld a, (current_level)
    ld e, a
    ld a, (de)
    ld l, 1
    ld (hl), a
    ld a, (next_lev_flag)
    ld l, a
    ld a, (hl)

    ; if current_level == last_level, current_level = starting_level else current_level = current_level
    ld l, a
    ld (hl), a
    ld l, last_level
    ld (hl), starting_level
    ld l, a
    ld a, (hl)
    ld (current_level), a

    ; more variables
    ld a, 0
    ld (food_flag), a


if 1
    ld hl, $0100 ; tail = 1, head = 0
    ld (head_and_tail_ptr), hl

    ld h, HIGH snake_array
    ld (hl), 232 ; starting head pos
    ld l, 1
    ld (hl), 248 ; starting tail pos
else
    ld hl, $0800 ; tail = 1, head = 0
    ld (head_and_tail_ptr), hl

    ld h, HIGH snake_array
    ld (hl), 144 ; starting head pos
    ld l, 1
    ld (hl), 143
    ld l, 2
    ld (hl), 142
    ld l, 3
    ld (hl), 141
    ld l, 4
    ld (hl), 140
    ld l, 5
    ld (hl), 139
    ld l, 6
    ld (hl), 138
    ld l, 7
    ld (hl), 137
    ld l, 8
    ld (hl), 136
endif

    ld a, HIGH up_tab ; start facing up
    ld (direction), a

    ld hl, LAST_K
    ld (hl), 0 ; reset keypress

    ld a, 1
    ld (food_flag), a ; get the food to draw

    ld a, goto_loop
    ld (die_sp), a

    ld a, 50 ; how much you have to eat before you can enter the next level
    ld (next_lev_counter), a

    ld a, 0
    ld (next_lev_flag), a

    ld a, food_colour
    ld (draw_food_colour), a

rept gameloop - * ; DUMMY INSTRUCTIONS
    ld a, a
mend

org gameloop
    ld sp, goto_stack

    ld a, 0
    ld (grow_flag), a

    ; read keyboard, generate new direction and position
    ld hl, LAST_K
    ld l, (hl) ; get index into table
    ld h, HIGH key_tab
    ld e, (hl)

    ; prevent 180 degree turns
    ; if (keypress == opposite[dir])
    ;     keypress = dir
    ; if (keypress != none)
    ;     dir = keypress

    ld a, (direction)
    ld l, a
    ld h, HIGH opposite_dir_tab
    ld l, (hl)

    ld h, HIGH scratch
    ld d, h

    ld (hl), e ; *opposite = keypress
    ld (de), a ; *keypress = dir
    ld l, (hl)

    ; if keypress == opposite: L=dir else L=keypress
    ld e, no_key
    ld (hl), l ; *keypress = keypress
    ld (de), a ; *none = dir

    ; if keypress == none: dir else keypress
    ld a, (hl) ; get dir
    ld (direction), a ; save it
    ld hl, (head_and_tail_ptr)
    ld h, HIGH snake_array
    ld e, (hl) ; current pos
    ld d, a ; index into direction table
    ld a, (de) ; new position
    ld (temp_pos), a ; save it

    ; now check if we've hit something
    ld ixl, a
    ld ixh, HIGH screen_tab
    ld l, (ix+0)
    ld ixh, (HIGH screen_tab) + 1
    ld h, (ix+0)
    ld e, (hl) ; ATTRs of screen

    ld d, HIGH scratch
    ld h, d

    ld a, goto_loop
    ld (de), a

    ; repeat for more obstacle types
    ld l, obstacle_colour
    ld (hl), goto_main
    ld l, snake_colour
    ld (hl), goto_main
    ld l, next_lev_colour
    ld (hl), goto_main

    ld a, (de)
    ld (die_sp), a

    ld a, 0
    ld (de), a
    ld (hl), 1
    ld a, (de) ; ATTR == next_lev_colour
    ld l, a
    ld d, a

    ; we want to keep the flag set if it is already
    ld (hl), 1
    ld a, (next_lev_flag)
    ld l, a
    ld (hl), 0
    ld l, 1
    ld (hl), l
    ld l, d
    ld a, (hl)
    ld (next_lev_flag), a

    ld l, food_colour
    ld d, h

    ld a, 0
    ld (de), a

    ld (hl), 1
    ld a, (de) ; ATTR == food
    ld (grow_flag), a
    ld c, a

    ; we want to keep the flag set if it is already
    ld e, a
    ld a, 1
    ld (de), a ; don't change DE
    ld a, (food_flag)
    ld l, a
    ld (hl), 0
    ld l, 1
    ld (hl), l
    ld a, (de)
    ld (food_flag), a

    ; now increment score
    ld a, (new_hi)
    ld l, a
    ld a, (score)
    ld e, a
    ld a, 0
    ld (de), a
    ld (hl), 1
    ld a, (de) ; score == new_hi
    ld h, HIGH inc_tab
    ld b, (hl) ; new_hi + 1

    ld e, c ; attr == food
    ld c, l ; new_hi
    ld h, d ; scratch
    ld l, a ; score == new_hi

    ld a, c
    ld (de), a
    ld (hl), b
    ld l, 0
    ld (hl), a
    ld a, (de)
    ld (new_hi), a

    ld a, (score)
    ld (hl), a
    ld l, 1
    ld b, HIGH inc_tab
    ld c, a
    ld a, (bc)
    ld (hl), a ; score + 1
    ld a, (grow_flag)
    ld l, a
    ld a, (hl)
    ld (score), a

    ; approx 660 Ts so far
    ; 144 bytes (at least)

    ; current registers:
    ; A = score
    ; B = inc_tab
    ; C = old_score
    ; D = scratch
    ; E = food_flag == (ATTR == food)
    ; H = scratch
    ; L = grow_flag

    ; if grow flag, draw block at tail else draw empty
    ld c, l ; save grow_flag
    ld l, 0
    ld (hl), empty_colour
    ld l, 1
    ld (hl), snake_colour
    ld l, c ; restore grow_flag
    ld e, (hl) ; save colour

    ld a, (tail)
    ld c, a
    ld l, a
    ld h, HIGH snake_array
    ld a, (hl) ; tail pos

    ld ixl, a
    ld ixh, HIGH screen_tab
    ld l, (ix+0)
    ld ixh, (HIGH screen_tab) + 1
    ld h, (ix+0)
    ld (hl), e ; draw tail

    ; only want to decrement tail if grow_flag == 0
    ld h, HIGH scratch
    ld l, 1
    ld (hl), c
    ld l, 0
    ld b, HIGH dec_tab
    ld a, (bc)
    ld (hl), a ; tail - 1
    ld a, (grow_flag)
    ld l, a
    ld a, (hl)
    ld (tail), a

    ; now draw head colour
    ld a, (head)
    ld c, a
    ld l, a
    ld h, HIGH snake_array
    ld a, (hl) ; head pos

    ld ixl, a
    ld ixh, HIGH screen_tab
    ld l, (ix+0)
    ld ixh, (HIGH screen_tab) + 1
    ld h, (ix+0)
    ld (hl), snake_colour
    ld a, (bc) ; head - 1
    ld (head), a

    ld l, a
    ld h, HIGH snake_array
    ld a, (temp_pos)
    ld (hl), a

    ld ixl, a
    ld ixh, HIGH screen_tab
    ld l, (ix+0)
    ld ixh, (HIGH screen_tab) + 1
    ld h, (ix+0)
    ld (hl), head_colour

    ; if just ate food (grow_flag == 1) then decrement next_lev_counter
    ld h, HIGH dec_tab
    ld a, (next_lev_counter)
    ld e, a
    ld l, a
    ld a, (hl)

    ld h, HIGH scratch
    ld l, 1
    ld (hl), a
    ld l, 0
    ld (hl), e

    ld a, (grow_flag)
    ld l, a
    ld a, (hl)
    ld (next_lev_counter), a

    ld l, 0
    ld (hl), food_colour
    ld l, a
    ld (hl), next_lev_colour
    ld l, 0
    ld a, (hl)

    ld (draw_food_colour), a

    ; now print current score
    ;
    ; use digit_tab to convert a number
    ; into three low-byte addresses to fonts.
    ; 11 chars of 8 bytes each = 88 bytes
    ;
    ; can we use the ROM font?

    ; registers so far:
    ; A = temp_pos
    ; B = dec_tab
    ; C = head_ptr
    ; D = scratch
    ; E = colour
    ; H = screen
    ; L = temp_pos

    ld a, (score)
    ld b, font
    ld h, HIGH inc_tab
    ld e, a
    ld d, HIGH digit_tab
    ld a, (de) ; get first digit
    ld c, a

    ; print first char
    ld a, (bc)
    ld (score_screen), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + $100), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + $200), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + $300), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + $400), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + $500), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + $600), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + $700), a

    ld d, (HIGH digit_tab) + 1
    ld a, (de) ; get second digit
    ld c, a

    ; print second char
    ld a, (bc)
    ld (score_screen + 1), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 1 + $100), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 1 + $200), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 1 + $300), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 1 + $400), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 1 + $500), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 1 + $600), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 1 + $700), a

    ld d, (HIGH digit_tab) + 2
    ld a, (de) ; get third digit
    ld c, a

    ; print third char
    ld a, (bc)
    ld (score_screen + 2), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 2 + $100), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 2 + $200), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 2 + $300), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 2 + $400), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 2 + $500), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 2 + $600), a
    ld l, c
    ld c, (hl)

    ld a, (bc)
    ld (score_screen + 2 + $700), a

    ; now if food_flag, draw food
    ; find next food pos
    ; draw food:

    ; current registers:
    ; A = last score graphic
    ; B = font
    ; C = lo_font
    ; D = digit_tab + 2
    ; E = score
    ; H = inc_tab
    ; L = low_font - 1

    ld a, (rand_ptr)
    ld l, a
    ld a, (hl) ; increment rand_ptr
    ld (rand_ptr), a ; and save it back
    ld h, HIGH rand_tab
    ld l, a
    ld a, (hl) ; get a random position

    ld ixl, a
    ld ixh, HIGH screen_tab
    ld e, (ix+0)
    ld ixh, (HIGH screen_tab) + 1
    ld d, (ix+0) ; construct screen adr in DE

    ; don't modify DE now - we need it later
    ld h, HIGH scratch
    ld a, (de) ; get ATTR byte at potential food pos
    ld l, a
    ld (hl), 0 ; is_not_empty
    ld l, empty_colour
    ld (hl), 1 ; is_empty
    ld l, a ; background colour

    ld b, (hl) ; is_empty
    ld c, a ; background
    ld l, b
    ld (hl), c ; background
    ld a, (food_flag)
    ld l, a
    ld a, (draw_food_colour)
    ld (hl), a
    ld a, l
    ld l, 0
    ld (hl), c ; background
    ld c, a ; food_flag
    ld l, b ; is_empty
    ld a, (hl)
    ld (de), a ; draw it

    ; invert is_empty
    ld l, 0
    ld (hl), l
    ld l, b ; is_empty
    ld (hl), 1
    ld l, 0
    ld b, (hl) ; !is_empty

    ld l, b
    ld (hl), 0
    ld l, c
    ld (hl), 1
    ld l, 0
    ld (hl), 0
    ld l, b
    ld a, (hl)
    ld (food_flag), a

rept delayloop - * ; DUMMY INSTRUCTIONS
    ld a, a
mend

org delayloop
    ld a, (delay_counter)
    ld h, HIGH dec_tab
    ld l, a
    ld l, (hl)
    ld h, HIGH scratch
    ld d, h

    ; if condition:
    ;     timer = max_time
    ;        sp = goto_loop
    ; else:
    ;     timer = new_timer (E)
    ;        sp = goto_delayloop

    ld e, 0
    ld a, (delay_length)
    ld (hl), l
    ld (de), a
    ld a, (hl)
    ld (delay_counter), a
    ld (hl), goto_delayloop
    ld a, (die_sp)
    ld (de), a
    ld a, (hl)

    ld h, 0
    ld l, a
    ld sp, hl

    ld hl, $4000 ; in contended memory to make 100% sure we never run out of RAM

rept 65336 - * ; DUMMY INSTRUCTIONS
    ld a, (hl)
mend

loop_end:

Zeus_PC equ start

output_bin "../bin/build/ir/game.bin", start, loop_end - start
