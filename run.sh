#!/usr/bin/env bash

BUILD_DIR="bin/build"

# get path of emulator to run
# NOTE: emupath.txt MUST be formatted with Unix
# linefeeds (LF)!!!
read -r emupath < emupath.txt

echo Using emulator $emupath

# get name of current build
git_version=$(git describe --long --tags --dirty --always)
build_name="snake-${git_version}"

build_file="${BUILD_DIR}/${build_name}.tap"

echo Using file $build_file
echo

# check that build exists
if ! [ -f "$build_file" ]; then
    . ./build.sh $1
fi

echo
echo Launching emulator...

# launch emulator in background
$emupath "$(pwd)/$build_file" > /dev/null 2>&1 &
