#!/usr/bin/env bash

DL_DIR='bin'
ZEUS_DIR='tools/zeus'
BAS2TAP_DIR='tools/bas2tap'
GENTAPE_DIR='tools/GenTape'

ZEUS_URL='http://www.desdes.com/products/oldfiles/zeus.zip'
BAS2TAP_URL='http://www.worldofspectrum.org/pub/sinclair/tools/pc/bas2tap25-win.zip'
GENTAPE_URL='http://retrolandia.net/foro/attachment.php?aid=345'

if ! [ -d "$DL_DIR" ]; then
    mkdir -p "$DL_DIR"
fi


#### ZEUS ####

if ! [ -d "$ZEUS_DIR" ]; then
    mkdir -p "$ZEUS_DIR"
fi

echo Downloading latest build of Zeus...
echo

curl -L "$ZEUS_URL" > "$DL_DIR"/zeus.zip

echo
echo Unpacking Zeus...
echo

# extract zeus
unzip -o "${DL_DIR}/zeus.zip" -d"$ZEUS_DIR"

echo
echo Done.
echo

#### BAS2TAP ####

if ! [ -d "$BAS2TAP_DIR" ]; then
    mkdir -p "$BAS2TAP_DIR"
fi

echo Downloading latest build of bas2tap...
echo

curl -L "$BAS2TAP_URL" > "$DL_DIR"/bas2tap.zip

echo
echo Unpacking bas2tap...
echo

echo
echo Done.
echo

# extract bas2tap
unzip -o "${DL_DIR}/bas2tap.zip" -d"$BAS2TAP_DIR"

#### GENTAPE ####

if ! [ -d "$GENTAPE_DIR" ]; then
    mkdir -p "$GENTAPE_DIR"
fi

echo Downloading latest build of GenTape...
echo

curl -L "$GENTAPE_URL" > "$DL_DIR"/GenTape.zip

echo
echo Unpacking GenTape...
echo

# extract GenTape
unzip -o "${DL_DIR}/GenTape.zip" -d"$GENTAPE_DIR"

echo
echo Setup complete!
